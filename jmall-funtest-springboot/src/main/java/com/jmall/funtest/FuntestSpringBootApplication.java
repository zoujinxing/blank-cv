package com.jmall.funtest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * <br/>Date 2022/4/12
 * <br/>Time 12:43:33
 *
 * @author _blank
 */
@SpringBootApplication
public class FuntestSpringBootApplication {

    @Autowired
    private DistributedLock distributedLock;
//    @Autowired
//    private Lock lock;

    public static void main(String[] args) {
        SpringApplication.run(FuntestSpringBootApplication.class, args);
    }

}

@Configuration
class Config {
    @Bean
    public Lock lock() {
        return new DistributedLock();
    }
}

interface Lock {
}

class DistributedLock implements Lock {

}
