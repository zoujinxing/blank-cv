package com.jmall.funtest.controller;

import com.jmall.funtest.entity.User;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * <br/>Date 2022/4/12
 * <br/>Time 12:51:50
 *
 * @author _blank
 */
@RestController
@Slf4j
@Tag(name = "user", description = "用户管理API")
public class UserController {

    //    @ApiResponses(value = {
//            @ApiResponse(
//                    description = "successful operation",
//                    content = {@Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = User.class))})
//    })
    @Operation(summary = "新增用户", description = "这只能由登录用户完成。", tags = {"user"})
    @PostMapping(path = "create-user")
    ResponseEntity<Void> createUser(@io.swagger.v3.oas.annotations.parameters.RequestBody(description = "123Created user object") @RequestBody User user) {
        log.info("user : {}", user.toString());
        return ResponseEntity.ok().build();
    }

}
