package com.jmall.funtest.entity;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

/**
 * TODO 描述
 * <br/>Date 2022/4/12
 * <br/>Time 12:53:08
 *
 * @author _blank
 */
@Data
@Schema(description = "用户实例")
public class User {
    @Schema(description = "用户ID", example = "1")
    private Long id;

    @Schema(description = "用户名")
    private String username;

    @Schema(description = "姓")
    private String firstName;

    @Schema(description = "名")
    private String lastName;

    @Schema(description = "电子邮件")
    private String email;

    @Schema(description = "密码")
    private String password;

    @Schema(name = "phone", description = "用户手机号")
    private String phone;

    @Schema(description = "用户状态")
    private Integer userStatus;
}
