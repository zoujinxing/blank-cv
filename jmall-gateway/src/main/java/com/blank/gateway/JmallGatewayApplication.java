package com.blank.gateway;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 网关应用
 * <br/>Date 2022/3/16
 * <br/>Time 23:30:20
 *
 * @author _blank
 */
@SpringBootApplication
public class JmallGatewayApplication {

    public static void main(String[] args) {
        SpringApplication.run(JmallGatewayApplication.class, args);
    }

}
