#!/bin/bash

# 单引号不能读到变量，双引号可以读到变量

echo "hello world!"

echo 'hello world!'

# cut 用法
cut --help

# awk 用法
awk --help

# sed 用法
sed --help

