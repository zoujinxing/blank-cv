### Start a _RocketMQ_ server instance

```shell
# github 仓库地址
# https://github.com/apache/rocketmq
# https://github.com/apache/rocketmq-dashboard
# https://github.com/apache/rocketmq-docker
# docker hub 现有镜像
# https://hub.docker.com/u/apacherocketmq
# 官网发行包下载
# https://rocketmq.apache.org/dowloading/releases/
```

#### 创建RocketMQ工作目录

```shell
mkdir -p /data/rocketmq.apache.org/rocketmq-standalone/{namesrv/logs,broker/logs,broker/store}
```

#### Dockerfile for rocketmq

```shell

```