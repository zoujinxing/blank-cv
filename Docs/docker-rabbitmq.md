### Start a _rabbitmq_ server instance

```shell
# hub: https://hub.docker.com/_/rabbitmq
# dockerfile: https://github.com/docker-library/rabbitmq

$ mkdir -p /data/rabbitmq.com/rabbitmq-standalone/{etc/conf.d,home_dir,logs}

$ touch /data/rabbitmq.com/rabbitmq-standalone/hosts
# network为host模式下，需要手动改下hosts映射，避免连接不上问题
$ echo 127.0.0.1    rabbitmq-standalone > /data/rabbitmq.com/rabbitmq-standalone/hosts

$ chown -R 999:999 /data/rabbitmq.com/rabbitmq-standalone/

$ docker run \
      --hostname rabbitmq-standalone \
      --log-driver local \
      --log-opt max-size=128m \
      --name rabbitmq-standalone \
      --network host \
      -d \
      -e "TZ=Asia/Shanghai" \
      -v /etc/localtime:/etc/localtime:ro \
      -v /data/rabbitmq.com/rabbitmq-standalone/hosts:/etc/hosts \
      -v /data/rabbitmq.com/rabbitmq-standalone/etc/enabled_plugins:/etc/rabbitmq/enabled_plugins \
      -v /data/rabbitmq.com/rabbitmq-standalone/etc/conf.d:/etc/rabbitmq/conf.d \
      -v /data/rabbitmq.com/rabbitmq-standalone/home_dir:/var/lib/rabbitmq \
      -v /data/rabbitmq.com/rabbitmq-standalone/logs:/var/log/rabbitmq \
      rabbitmq:3.9

$ docker cp 3d5d9788ffd7:/etc/rabbitmq/enabled_plugins /data/rabbitmq.com/rabbitmq-standalone/etc
$ docker cp 3d5d9788ffd7:/etc/rabbitmq/conf.d/ /data/rabbitmq.com/rabbitmq-standalone/etc/
```
