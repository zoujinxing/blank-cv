## 常用docker镜像运行命令备忘

> [Docker命令实战](https://www.yuque.com/leifengyang/oncloud/ox16bw)

### Start a _consul_ server instance
```shell
$ mkdir -p /data/consul.io/consul-standalone
```

### Start a _elasticsearch_ server instance
```shell
# 参考官方：https://www.elastic.co/guide/en/elasticsearch/reference/current/docker.html
# 参考文章：https://stackoverflow.com/questions/64402802/how-to-use-multiple-command-in-docker-compose-cli/64403490

# 1、创建elasticsearch外部挂载的目录
$ mkdir -p /data/elastic.co/elasticsearch-standalone/{config/jvm.options.d,data,logs,plugins}
# 2、修改成docker es需要的目录权限
$ chown -R 1000:root /data/elastic.co/elasticsearch-standalone/
# $ chmod -R g+w /data/elastic.co/elasticsearch-standalone/
# $ chgrp -R 0 /data/elastic.co/elasticsearch-standalone/

# 插件地址
# https://github.com/NLPchina/elasticsearch-analysis-ansj/releases
# https://github.com/medcl/elasticsearch-analysis-ik/releases
# https://github.com/medcl/elasticsearch-analysis-pinyin/releases
# 解压插件到plugins的命令
# $ unzip -q /data/elastic.co/elasticsearch-standalone/plugins/elasticsearch-analysis-ansj-7.9.3.0-release.zip -d /data/elastic.co/elasticsearch-standalone/plugins/elasticsearch-analysis-ansj-7.9.3 \
#       && unzip -q /data/elastic.co/elasticsearch-standalone/plugins/elasticsearch-analysis-ik-7.16.3.zip -d /data/elastic.co/elasticsearch-standalone/plugins/elasticsearch-analysis-ik-7.16.3 \
#       && unzip -q /data/elastic.co/elasticsearch-standalone/plugins/elasticsearch-analysis-pinyin-7.16.3.zip -d /data/elastic.co/elasticsearch-standalone/plugins/elasticsearch-analysis-pinyin-7.16.3

# 可以不进入容器执行些命令，例如：
# $ docker exec -t 5f8a92f7d563 bash "-c" "ls -alh / && echo && ls -alh ."

# 3、运行docker es单机版本
$ docker run \
      --group-add 0 \
      --log-driver local \
      --log-opt max-size=128m \
      --name elasticsearch-standalone \
      --network host \
      --ulimit nofile=65535:65535 \
      --ulimit memlock=-1:-1 \
      -d \
      -e "discovery.type=single-node" \
      -e "TZ=Asia/Shanghai" \
      -v /etc/localtime:/etc/localtime:ro \
      -v /data/elastic.co/elasticsearch-standalone/config/jvm.options.d/:/usr/share/elasticsearch/config/jvm.options.d/:ro \
      -v /data/elastic.co/elasticsearch-standalone/config/elasticsearch.yml:/usr/share/elasticsearch/config/elasticsearch.yml:ro \
      -v /data/elastic.co/elasticsearch-standalone/data:/usr/share/elasticsearch/data \
      -v /data/elastic.co/elasticsearch-standalone/logs:/usr/share/elasticsearch/logs \
      -v /data/elastic.co/elasticsearch-standalone/plugins:/usr/share/elasticsearch/plugins \
      docker.elastic.co/elasticsearch/elasticsearch:7.16.3

# https://www.elastic.co/guide/en/elasticsearch/reference/7.16/security-minimal-setup.html#_enable_elasticsearch_security_features
# 4、为docker elasticsearch使用 auto 参数将随机生成的密码输出到控制台
$ docker exec -it b72ff8ce6d54 bash "-c" "./bin/elasticsearch-setup-passwords auto"

# Initiating the setup of passwords for reserved users elastic,apm_system,kibana,kibana_system,logstash_system,beats_system,remote_monitoring_user.
# The passwords will be randomly generated and printed to the console.
# Please confirm that you would like to continue [y/N]y
# 
# 
# Changed password for user apm_system
# PASSWORD apm_system = 8i4cL7TUjbz7botWOrBM
# 
# Changed password for user kibana_system
# PASSWORD kibana_system = akbG5B9lw3tmlhY0fYbV
# 
# Changed password for user kibana
# PASSWORD kibana = akbG5B9lw3tmlhY0fYbV
# 
# Changed password for user logstash_system
# PASSWORD logstash_system = YzG3ny3th4yInmNjZqhd
# 
# Changed password for user beats_system
# PASSWORD beats_system = kTFenDoalX1oVRA5BLvt
# 
# Changed password for user remote_monitoring_user
# PASSWORD remote_monitoring_user = VIKWzOFkXqE2IO8rtX6x
# 
# Changed password for user elastic
# PASSWORD elastic = oDZSfyPIQhUUTUHT14Nq
```

### Start a _kibana_ server instance
```shell
$ docker run \
      --log-driver local \
      --log-opt max-size=128m \
      --name kibana-standalone \
      --network host \
      -d \
      -e "ELASTICSEARCH_HOSTS=http://127.0.0.1:9200" \
      -e "TZ=Asia/Shanghai" \
      -v /etc/localtime:/etc/localtime:ro \
      docker.elastic.co/kibana/kibana:7.16.3

# https://www.elastic.co/guide/en/elasticsearch/reference/7.16/security-minimal-setup.html#add-built-in-users
# 配置 Kibana 以使用密码连接到 Elasticsearch
# 1、将 elasticsearch.username 设置添加到 KIB_PATH_CONF/kibana.yml 文件并将值设置为 kibana_system 用户
$ docker exec -t 39d6adbdc9b4 bash "-c" "cat ./config/kibana.yml && echo>>./config/kibana.yml && echo '# 设置连接es的账户名'>>./config/kibana.yml && echo 'elasticsearch.username: \"kibana_system\"'>>./config/kibana.yml && echo && echo && cat ./config/kibana.yml"
# 2、在安装 Kibana 的目录中，运行以下命令来创建 Kibana 密钥库并添加安全设置：
$ docker exec -it 39d6adbdc9b4 bash "-c" "./bin/kibana-keystore create && ./bin/kibana-keystore add elasticsearch.password"
# 3、重启 Kibana。例如，如果您使用 .tar.gz 包安装 Kibana，请从 Kibana 目录运行以下命令：
$ docker restart 39d6adbdc9b4
```

### Start a _grafana_ server instance
```shell
$ mkdir -p /data/grafana.com/grafana-standalone
```

### Start a _jenkins_ server instance
```shell
$ mkdir -p /data/jenkins.io/jenkins_home_standalone/

$ chown -R 1000:1000 /data/jenkins.io/jenkins_home_standalone/

# https://www.jenkins.io/zh/doc/book/installing/#docker
# https://www.jenkins.io/doc/book/installing/docker/#downloading-and-running-jenkins-in-docker

# 创建Jenkins桥接网络
# $ docker network create jenkins

$ docker run \
      --log-driver local \
      --log-opt max-size=128m \
      --name jenkins-standalone \
      --network host \
      -d \
      -e "JAVA_OPTS=-Xms512m -Xmx512m" \
      -e "TZ=Asia/Shanghai" \
      -v /etc/localtime:/etc/localtime:ro \
      -v /var/run/docker.sock:/var/run/docker.sock \
      -v /data/jenkins.io/jenkins_home_standalone:/var/jenkins_home \
      jenkins/jenkins:2.319.2-lts-jdk11
#      -p 48080:8080 \
#      -p 50000:50000 \
#      -u 0:0 \
```

### Start a _mongodb_ server instance
```shell
$ mkdir -p /data/mongodb.com/mongodb-standalone
```

### Start a _mysql_ server instance
```shell
$ mkdir -p /data/mysql.com/{mysql-5.7-standalone,mysql-8.0-standalone}

$ mkdir -p /data/mysql.com/mysql-5.7-standalone/{conf.d,data,logs}

$ chown -R 999:999 /data/mysql.com/mysql-5.7-standalone/

$ docker run \
      --log-driver local \
      --log-opt max-size=128m \
      --name mysql-standalone \
      --network host \
      -d \
      -e MYSQL_ROOT_PASSWORD=6969285 \
      -e "TZ=Asia/Shanghai" \
      -v /etc/localtime:/etc/localtime:ro \
      -v /data/mysql.com/mysql-5.7-standalone/data/:/var/lib/mysql \
      -v /data/mysql.com/mysql-5.7-standalone/logs/:/var/log/mysql \
      -v /data/mysql.com/mysql-5.7-standalone/conf.d/:/etc/mysql/conf.d \
      mysql:5.7

# cli进入mysql终端
$ docker exec -it e968187e46a7 bash "-c" "/usr/bin/mysql -uroot -p"
```

### Start a _nacos_ server instance
```shell
$ mkdir -p /data/nacos.io/nacos-standalone/{conf,init.d,logs}

$ touch /data/nacos.io/nacos-standalone/init.d/custom.properties

$ docker run \
      --log-driver local \
      --log-opt max-size=128m \
      --name nacos-standalone \
      --network host \
      -d \
      -e JVM_XMS=512m \
      -e JVM_XMX=512m \
      -e JVM_XMN=256m \
      -e MODE=standalone \
      -e SPRING_DATASOURCE_PLATFORM=mysql \
      -e "TZ=Asia/Shanghai" \
      -v /etc/localtime:/etc/localtime:ro \
      -v /data/nacos.io/nacos-standalone/conf/application.properties:/home/nacos/conf/application.properties:ro \
      -v /data/nacos.io/nacos-standalone/init.d/custom.properties:/home/nacos/init.d/custom.properties:ro \
      -v /data/nacos.io/nacos-standalone/logs:/home/nacos/logs \
      nacos/nacos-server:2.0.3-slim

# 复制配置文件
$ docker cp 1e9912be2fab:/home/nacos/conf/application.properties /data/nacos.io/nacos-standalone/conf/
```

### Start a _nginx_ server instance
```shell
$ mkdir -p /data/nginx.org/nginx-standalone/{conf.d,html,logs}

$ chown -R 101:101 /data/nginx.org/nginx-standalone/

$ docker run \
      --log-driver local \
      --log-opt max-size=128m \
      --name nginx-standalone \
      --network host \
      -d \
      -e "TZ=Asia/Shanghai" \
      -v /etc/localtime:/etc/localtime:ro \
      -v /data/nginx.org/nginx-standalone/conf.d/:/etc/nginx/conf.d \
      -v /data/nginx.org/nginx-standalone/html/:/usr/share/nginx/html:ro \
      -v /data/nginx.org/nginx-standalone/logs/:/var/log/nginx \
      -v /data/nginx.org/static/:/opt/static:ro \
      nginx:1.21

# 复制基础配置文件
$ docker cp f5f6d9d6f338:/etc/nginx/conf.d /data/nginx.org/nginx-standalone/
# 复制基础静态页面
$ docker cp f5f6d9d6f338:/usr/share/nginx/html /data/nginx.org/nginx-standalone/

# 更新重启后自动启动策略
$ docker update --restart=on-failure:3 f5f6d9d6f338
$ docker update --restart=always f5f6d9d6f338
```

### Start a _portainer_ server instance
```shell
$ mkdir -p /data/portainer.io/portainer-standalone

$ docker run \
      --log-driver local \
      --log-opt max-size=128m \
      --name portainer-standalone \
      --network host \
      --restart=always \
      -d \
      -e "TZ=Asia/Shanghai" \
      -v /etc/localtime:/etc/localtime:ro \
      -v /var/run/docker.sock:/var/run/docker.sock \
      -v /data/portainer.io/portainer-standalone:/data \
      portainer/portainer-ce:2.11.0-alpine
```

### Start a _rabbitmq_ server instance
```shell
# hub: https://hub.docker.com/_/rabbitmq
# dockerfile: https://github.com/docker-library/rabbitmq

$ mkdir -p /data/rabbitmq.com/rabbitmq-standalone/{etc/conf.d,home_dir,logs}

$ touch /data/rabbitmq.com/rabbitmq-standalone/hosts
# network为host模式下，需要手动改下hosts映射，避免连接不上问题
$ echo 127.0.0.1    rabbitmq-standalone > /data/rabbitmq.com/rabbitmq-standalone/hosts

$ chown -R 999:999 /data/rabbitmq.com/rabbitmq-standalone/

$ docker run \
      --hostname rabbitmq-standalone \
      --log-driver local \
      --log-opt max-size=128m \
      --name rabbitmq-standalone \
      --network host \
      -d \
      -e "TZ=Asia/Shanghai" \
      -v /etc/localtime:/etc/localtime:ro \
      -v /data/rabbitmq.com/rabbitmq-standalone/hosts:/etc/hosts \
      -v /data/rabbitmq.com/rabbitmq-standalone/etc/enabled_plugins:/etc/rabbitmq/enabled_plugins \
      -v /data/rabbitmq.com/rabbitmq-standalone/etc/conf.d:/etc/rabbitmq/conf.d \
      -v /data/rabbitmq.com/rabbitmq-standalone/home_dir:/var/lib/rabbitmq \
      -v /data/rabbitmq.com/rabbitmq-standalone/logs:/var/log/rabbitmq \
      rabbitmq:3.9

$ docker cp 3d5d9788ffd7:/etc/rabbitmq/enabled_plugins /data/rabbitmq.com/rabbitmq-standalone/etc
$ docker cp 3d5d9788ffd7:/etc/rabbitmq/conf.d/ /data/rabbitmq.com/rabbitmq-standalone/etc/
```

### Start a _redis_ server instance
```shell
$ mkdir -p /data/redis.io/redis-6.2-standalone/{conf.d,data,logs,modules}

$ chown -R 999:999 /data/redis.io/redis-6.2-standalone/

# --loadmodule /usr/lib/redis/modules/redisearch.so
# --loadmodule /usr/lib/redis/modules/rejson.so

$ docker run \
      --log-driver local \
      --log-opt max-size=128m \
      --name redis-standalone \
      --network host \
      -d \
      -e "TZ=Asia/Shanghai" \
      -v /etc/localtime:/etc/localtime:ro \
      -v /data/redis.io/redis-6.2-standalone/conf.d:/usr/local/etc/redis \
      -v /data/redis.io/redis-6.2-standalone/data:/data \
      -v /data/redis.io/redis-6.2-standalone/logs:/usr/local/logs/redis \
      -v /data/redis.io/redis-6.2-standalone/modules:/usr/local/modules/redis \
      redis:6.2 \
      /usr/local/bin/redis-server /usr/local/etc/redis/redis.conf

# 控制台进入redis-cli
$ docker exec -it bc94090f40ed bash "-c" "/usr/local/bin/redis-cli"
```

### Start a _sonatype-nexus_ server instance
```shell
# https://hub.docker.com/r/sonatype/nexus

$ mkdir -p /data/sonatype.com/nexus-repository-manager-2-oss-standalone/{conf,logs,sonatype-work}

$ chown -R 200:200 /data/sonatype.com/nexus-repository-manager-2-oss-standalone/

$ docker cp 2259113786a8:/opt/sonatype/nexus/conf /data/sonatype.com/nexus-repository-manager-2-oss-standalone/
$ docker cp 2259113786a8:/opt/sonatype/nexus/logs /data/sonatype.com/nexus-repository-manager-2-oss-standalone/

$ docker run \
      --log-driver local \
      --log-opt max-size=128m \
      --name nexus-2-oss-standalone \
      --network host \
      -d \
      -e "TZ=Asia/Shanghai" \
      -e MIN_HEAP=512m \
      -e MAX_HEAP=512m \
      -v /etc/localtime:/etc/localtime:ro \
      -v /data/sonatype.com/nexus-repository-manager-2-oss-standalone/conf:/opt/sonatype/nexus/conf \
      -v /data/sonatype.com/nexus-repository-manager-2-oss-standalone/logs:/opt/sonatype/nexus/logs \
      -v /data/sonatype.com/nexus-repository-manager-2-oss-standalone/sonatype-work:/sonatype-work \
      sonatype/nexus:2.14.21-02

# Default credentials are: admin / admin123

# 在nginx中反向代理nexus的配置参考（注意修改 Webapp Context Path 配置文件）：
# https://help.sonatype.com/repomanager2/installing-and-running/running-behind-a-reverse-proxy
```

### Start a _zookeeper_ server instance

```shell
# https://hub.docker.com/_/zookeeper
# https://blog.csdn.net/weixin_33921089/article/details/91923344
# https://zookeeper.apache.org/doc/r3.5.7/zookeeperAdmin.html#sc_adminserver_config

$ mkdir -p /data/zookeeper.apache.org/zookeeper-standalone/{conf,data,datalog,logs}

$ chown -R 1000:1000 /data/zookeeper.apache.org/zookeeper-standalone/

$ docker cp ddd5afcd9385:/conf/ /data/zookeeper.apache.org/zookeeper-standalone/

$ docker run \
      --log-driver local \
      --log-opt max-size=128m \
      --name zookeeper-standalone \
      --network host \
      -d \
      -e JVMFLAGS="-Xms512m -Xmx512m -Dzookeeper.serverCnxnFactory=org.apache.zookeeper.server.NettyServerCnxnFactory" \
      -e "TZ=Asia/Shanghai" \
      -e ZOO_LOG4J_PROP="INFO,ROLLINGFILE" \
      -e JMXDISABLE="true" \
      -e USER="`whoami`" \
      -v /etc/localtime:/etc/localtime:ro \
      -v /data/zookeeper.apache.org/zookeeper-standalone/conf:/conf \
      -v /data/zookeeper.apache.org/zookeeper-standalone/data:/data \
      -v /data/zookeeper.apache.org/zookeeper-standalone/datalog:/datalog \
      -v /data/zookeeper.apache.org/zookeeper-standalone/logs:/logs \
      zookeeper:3.7

# 开启远程访问需加参数
#      -e JMXPORT="19970" \
#      -e JMXHOSTNAME="192.168.98.128" \

```

### Start a _xxx_ server instance
```shell
$ mkdir -p 
```

### Start a _xxx_ server instance
```shell
$ mkdir -p 
```

### Start a _xxx_ server instance
```shell
$ mkdir -p 
```

### Start a _xxx_ server instance
```shell
$ mkdir -p 
```

### Start a _FastDFS_ server instance
```shell
# fastdfs wiki    https://github.com/happyfish100/fastdfs/wiki

# c common functions library extracted from FastDFS.    https://github.com/happyfish100/libfastcommon
# FastDFS nginx module    https://github.com/happyfish100/fastdfs-nginx-module

# FastDFS Dockerfile network (网络版本)
# https://github.com/happyfish100/fastdfs/blob/master/docker/dockerfile_network/Dockerfile

# nginx编译安装和模块配置详解    https://www.cnblogs.com/makecode/articles/9996639.html
# nginx动态加载modules    https://docs.nginx.com/nginx/admin-guide/dynamic-modules/dynamic-modules/

# [nginx官方镜像添加第三方模块]Adding third-party modules to nginx official image
# https://github.com/nginxinc/docker-nginx/blob/master/modules/README.md
```

