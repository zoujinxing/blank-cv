#### OAuth 2.0 实战课 王新栋 京东资深架构师

[OAuth 2.0 实战课](https://time.geekbang.org/column/article/254518)  
[OAuth 2.0 实战课 github 代码](https://github.com/xindongbook/oauth2-code)  
[使用Spring Security搭建一套基于JWT的OAuth 2.0架构](https://github.com/JosephZhu1983/SpringSecurity101)

---

#### 微服务架构实战 160 讲 杨波 拍拍贷研发总监、资深架构师、微服务技术专家

[微服务架构实战 160 讲](https://time.geekbang.org/course/intro/100007001)  
[课件及源码地址 gitee](https://gitee.com/geektime-geekbang/oauth2lab)  
[课件及源码地址 github](https://github.com/spring2go/oauth2lab)

---

#### Spring Boot与Kubernetes云原生微服务实践

[Spring Boot 与 Kubernetes 云原生微服务实践](https://time.geekbang.org/course/intro/100031401)

---
