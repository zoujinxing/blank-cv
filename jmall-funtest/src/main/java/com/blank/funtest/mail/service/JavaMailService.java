package com.blank.funtest.mail.service;

import lombok.RequiredArgsConstructor;
import org.springframework.boot.autoconfigure.mail.MailProperties;
import org.springframework.mail.MailSender;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.stereotype.Service;

/**
 * <br/>Date 2022/3/16
 * <br/>Time 18:17:50
 *
 * @author _blank
 */
@Service
@RequiredArgsConstructor
public class JavaMailService {

    private final MailSender mailSender;
    private final MailProperties mailProperties;

    public void sendMessage(String to, String subject, String text) {
        final SimpleMailMessage templateMessage = new SimpleMailMessage();
        templateMessage.setFrom(this.mailProperties.getUsername());

        // 收件人
        templateMessage.setTo(to);
        // 主题
        templateMessage.setSubject(subject);
        // 正文
        templateMessage.setText(text);

        this.mailSender.send(templateMessage);
    }

    public void sendMessage(SimpleMailMessage... simpleMessages) {
        for (final SimpleMailMessage simpleMessage : simpleMessages) {
            simpleMessage.setFrom(this.mailProperties.getUsername());
        }
        this.mailSender.send(simpleMessages);
    }


}
