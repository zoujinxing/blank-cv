package com.blank.funtest.mail;

import com.blank.funtest.mail.service.JavaMailService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.mail.SimpleMailMessage;

import javax.annotation.Resource;

/**
 * 使用java开发邮件系统
 * <br/>Date 2022/3/16
 * <br/>Time 11:53:20
 *
 * @author _blank
 */
@Slf4j
@SpringBootApplication(exclude = {DataSourceAutoConfiguration.class})
public class JavaMailApplication implements ApplicationRunner, CommandLineRunner {

    /*
     * 自2.4.0开始，org.springframework.boot.context.config.ConfigFileApplicationListener#postProcessEnvironment 废弃，由以下类加载配置文件
     *
     *
     * 1、spring boot启动
     * org.springframework.boot.SpringApplication#run(java.lang.String...)
     * 加载到296行：
     * org.springframework.boot.SpringApplication#prepareEnvironment
     * 加载到338行：
     * org.springframework.boot.SpringApplicationRunListeners#environmentPrepared
     *
     *
     * 2、spring boot启动加载postprocess
     * 执行到93行：
     * org.springframework.boot.context.config.ConfigDataEnvironmentPostProcessor#postProcessEnvironment(org.springframework.core.env.ConfigurableEnvironment, org.springframework.boot.SpringApplication)
     *
     * 3、获取ConfigDataEnvironment对象
     * 加载到102行：
     * org.springframework.boot.context.config.ConfigDataEnvironmentPostProcessor#getConfigDataEnvironment
     * 加载到137行（构造ConfigDataEnvironment对象）:
     * org.springframework.boot.context.config.ConfigDataEnvironment#ConfigDataEnvironment
     *
     * ①加载到148行（初始化resolvers）：
     * org.springframework.boot.context.config.ConfigDataEnvironment#createConfigDataLocationResolvers
     * 初始化配置文件解析器
     * org.springframework.boot.context.config.ConfigDataLocationResolvers#ConfigDataLocationResolvers(org.springframework.boot.logging.DeferredLogFactory, org.springframework.boot.ConfigurableBootstrapContext, org.springframework.boot.context.properties.bind.Binder, org.springframework.core.io.ResourceLoader, java.util.List<java.lang.String>)
     * 得到构建配置文件解析器（properties和yml）
     * org.springframework.boot.context.config.StandardConfigDataLocationResolver#StandardConfigDataLocationResolver
     * 加载到88行(org.springframework.boot.context.config.StandardConfigDataLocationResolver)
     * org.springframework.core.io.support.SpringFactoriesLoader#loadFactories
     * 加载到153行（得到 org.springframework.core.io.support.SpringFactoriesLoader#FACTORIES_RESOURCE_LOCATION 定义的org\springframework\boot\spring-boot\2.6.3\spring-boot-2.6.3-sources.jar!\META-INF\spring.factories的properties和yaml）
     * org.springframework.core.io.support.SpringFactoriesLoader#loadSpringFactories
     *
     *
     * ②加载到153行（初始化contributors）：
     * org.springframework.boot.context.config.ConfigDataEnvironment#createContributors(org.springframework.boot.context.properties.bind.Binder)
     * 加载到193行（获取查找配置文件的路径）
     * org.springframework.boot.context.config.ConfigDataEnvironment#getInitialImportContributors
     *
     * 4、将获取的路径与数据进行处理
     * 加载到102行：
     * org.springframework.boot.context.config.ConfigDataEnvironment#processAndApply
     * 加载到227行：
     * org.springframework.boot.context.config.ConfigDataEnvironment#processInitial
     * 加载到240行：
     * org.springframework.boot.context.config.ConfigDataEnvironmentContributors#withProcessedImports
     * 加载到116行：
     * org.springframework.boot.context.config.ConfigDataImporter#resolveAndLoad
     * 加载到113行：
     * org.springframework.boot.context.config.ConfigDataLocationResolvers#resolve(org.springframework.boot.context.config.ConfigDataLocationResolver<?>, org.springframework.boot.context.config.ConfigDataLocationResolverContext, org.springframework.boot.context.config.ConfigDataLocation, org.springframework.boot.context.config.Profiles)
     * 加载到120行：
     * org.springframework.boot.context.config.ConfigDataEnvironmentContributors#asContributors
     */

    @Resource
    private JavaMailService javaMailService;

    public static void main(String[] args) {
        args = ArrayUtils.add(args, "--spring.config.name=java-mail");
        SpringApplication.run(JavaMailApplication.class, args);
    }

    @Override
    public void run(final ApplicationArguments args) throws Exception {
        final String to = "413210424@qq.com";
        final String subject = RandomStringUtils.randomPrint(6) + "您好！";
        final String text = RandomStringUtils.randomAlphanumeric(13) + "这事一封测试信！";
        this.javaMailService.sendMessage(to, subject, text);
    }

    @Override
    public void run(final String... args) throws Exception {
        final SimpleMailMessage[] simpleMailMessages = new SimpleMailMessage[9];

        for (int i = 0; i < simpleMailMessages.length; i++) {
            simpleMailMessages[i] = new SimpleMailMessage();
            simpleMailMessages[i].setTo("413210424@qq.com");
            simpleMailMessages[i].setSubject(RandomStringUtils.randomPrint(6) + "您好！");
            simpleMailMessages[i].setText(RandomStringUtils.randomAlphanumeric(13) + "这事一封测试信！");
        }

        this.javaMailService.sendMessage(simpleMailMessages);
    }

}
