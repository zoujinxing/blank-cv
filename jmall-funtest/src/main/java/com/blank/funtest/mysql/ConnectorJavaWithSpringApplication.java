package com.blank.funtest.mysql;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.mail.MailSenderAutoConfiguration;
import org.springframework.jdbc.core.JdbcTemplate;

import javax.annotation.Resource;

/**
 * SPI机制
 * java.util.ServiceLoader#load(java.lang.Class)
 * 或者
 * sun.misc.Service#providers(java.lang.Class)
 * <p>
 * java spring jdbc connector mysql
 * <br/>Date 2022/3/17
 * <br/>Time 15:28:14
 *
 * @author _blank
 */
@SpringBootApplication(exclude = {MailSenderAutoConfiguration.class})
public class ConnectorJavaWithSpringApplication implements ApplicationRunner {

    @Resource
    private JdbcTemplate jdbcTemplate;

    public static void main(String[] args) {
        args = ArrayUtils.add(args, "--spring.config.name=java-mysql");
        SpringApplication.run(ConnectorJavaWithSpringApplication.class, args);
    }

    @Override
    public void run(final ApplicationArguments args) throws Exception {
        this.jdbcTemplate.execute("create table "
                + RandomStringUtils.randomAlphabetic(6)
                + "("
                + "tid int not null auto_increment primary key comment '主键'"
                + ", tuname varchar(16) comment '用户名'"
                + ", tsex char(1) comment '性别'"
                + ")"
        );
    }

}
