if (location.href.indexOf("bilibili") > -1) {
    document.getElementsByClassName("bilibili-player-video-btn-widescreen").forEach(div => {
        if (div.className.indexOf("closed") === -1) {
            div.children.forEach(button => {
                if (button.className.indexOf("bilibili-player-iconfont-widescreen-on") > -1) {
                    button.click()
                }
            })
        }
    })
}


function changePageStyle() {
    try {
        if (location.href.indexOf("ixigua") > -1) {// 西瓜视频
            let divs = document.getElementsByClassName("xgplayer-control-item__entry")
            let div = divs[divs.length - 2];
            if (div.ariaLabel === "剧场模式") {
                div.click()
            }
        } else if (location.href.indexOf("bilibili") > -1) {// 哔哩哔哩动画
            document.getElementsByClassName("bilibili-player-video-btn-widescreen").forEach(div => {
                if (div.className.indexOf("closed") === -1) {
                    div.children.forEach(button => {
                        if (button.className.indexOf("bilibili-player-iconfont-widescreen-on") > -1) {
                            button.click()
                        }
                    })
                }
            });
        } else if (location.href.indexOf("youtube") > -1) {// YouTube
            let controlsDiv = document.getElementsByClassName("ytp-right-controls")[0];
            let buttons = controlsDiv.children[7];
            if (buttons.title === "剧场模式 (t)") {
                buttons.click();
            }
        } else if (location.href.indexOf("hlsplayer") > -1) {// hlsplayer播放器
            let div = document.getElementsByClassName("hp-body")[0].children[0].children[0];
            div.style = "width: unset !important;";
            div.children[3].style = "max-width: unset;height: unset;width: 92%;";
        }
    } catch (e) {
        console.error(e)
    }
}