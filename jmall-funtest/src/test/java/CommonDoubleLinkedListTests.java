import lombok.RequiredArgsConstructor;
import org.springframework.beans.BeanUtils;

import java.util.Arrays;
import java.util.List;

/**
 * <br/>Date 2022/6/20
 * <br/>Time 0:41:08
 *
 * @author _blank
 */
public class CommonDoubleLinkedListTests {

    public static class AckIi {
        private List<String> tags;

        public List<String> getTags() {
            return tags;
        }

        public void setTags(final List<String> tags) {
            this.tags = tags;
        }
    }

    public static class AckJj {
        private List<Integer> tags;

        public List<Integer> getTags() {
            return tags;
        }

        public void setTags(final List<Integer> tags) {
            this.tags = tags;
        }

        @Override
        public String toString() {
            return "AckJj{" + "tags=" + tags + '}';
        }
    }

    public static void main(String[] args) {
        final AckIi ackIi = new AckIi();
        ackIi.setTags(Arrays.asList("one", "two"));
        final AckJj ackJj = new AckJj();
        BeanUtils.copyProperties(ackIi, ackJj);
        System.out.println(ackJj);
        System.out.println(ackJj.getTags());
        System.out.println(ackJj.getTags().get(0));
        System.out.println(ackJj.getTags().get(0).equals(1));


        final DoubleLinkedList doubleLinkedList = new DoubleLinkedList();
        doubleLinkedList.addHead("赵六", "王五", "李四", "张三");
        doubleLinkedList.addTail("孙七", "周八", "吴九", "郑十");

        for (Node node = doubleLinkedList.head.next; node.next != null; node = node.next) {
            System.out.println(node);
        }
    }

    @RequiredArgsConstructor
    private static class Node {
        private final String name;
        private Node prev, next;

        @Override
        public String toString() {
            return this.name + "    @" + this.hashCode();
        }
    }

    private static class DoubleLinkedList {
        private final Node head = new Node("表头");/* 链表头 */
        private final Node tail = new Node("表尾");/* 链表尾 */

        public DoubleLinkedList() {// 初始化，首尾相连
            this.head.next = this.tail;
            this.tail.prev = this.head;
        }

        void addHead(Node node) {
            node.next = this.head.next;
            node.prev = this.head;
            this.head.next.prev = node;
            this.head.next = node;
        }

        void addTail(Node node) {
            node.prev = this.tail.prev;
            node.next = this.tail;
            this.tail.prev.next = node;
            this.tail.prev = node;
        }

        void addHead(String... nodeNames) {
            Arrays.asList(nodeNames).forEach(nodeName -> this.addHead(new Node(nodeName)));
        }

        void addTail(String... nodeNames) {
            Arrays.asList(nodeNames).forEach(nodeName -> this.addTail(new Node(nodeName)));
        }
    }
}
