package cn.blank.leetcode;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

/**
 * 请你设计并实现一个满足 LRU (最近最少使用) 缓存 约束的数据结构。
 * <br/>实现 LRUCache 类：
 * <br/>    LRUCache(int capacity) 以 正整数 作为容量capacity 初始化 LRU 缓存
 * <br/>    int get(int key) 如果关键字 key 存在于缓存中，则返回关键字的值，否则返回 -1 。
 * <br/>    void put(int key, int value)如果关键字key 已经存在，则变更其数据值value ；如果不存在，则向缓存中插入该组key-value 。如果插入操作导致关键字数量超过capacity ，则应该 逐出 最久未使用的关键字。
 * <br/>函数 get 和 put 必须以 O(1) 的平均时间复杂度运行。
 * <p>
 * <br/><a href="https://leetcode.cn/problems/lru-cache/">146. LRU 缓存</a>
 *
 * @author _blank    DateTime 2022/7/28 12:59:57
 */
public class AbLruCacheTests {

    public static void main(String[] args) {
//        final LRUCache obj = new LRUCache(3);
        final ZyLRUCache obj = new ZyLRUCache(3);

        obj.put(1, 123);
        obj.put(2, 123);
        obj.put(3, 123);
        System.out.println(obj.keySet());

        obj.put(4, 123);
        System.out.println(obj.keySet());

        obj.put(3, 123);
        System.out.println(obj.keySet());

        obj.put(3, 123);
        System.out.println(obj.keySet());

        obj.put(3, 123);
        System.out.println(obj.keySet());

        obj.put(5, 123);
        System.out.println(obj.keySet());
    }

    private static class LRUCache extends LinkedHashMap<Integer, Integer> {
        private final int capacity;

        public LRUCache(int capacity) {
            // accessOrder – the ordering mode - true for access-order, false for insertion-order
            super(capacity, 0.75F, true);
            this.capacity = capacity;
        }

        public int get(int key) {
            return super.getOrDefault(key, -1);
        }

        // 这个可不写
        public void put(int key, int value) {
            super.put(key, value);
        }

        @Override
        protected boolean removeEldestEntry(Map.Entry<Integer, Integer> eldest) {
            return size() > capacity;
        }
    }

    // -----------------------------------------------------------------------------------------------------------------

    private static class ZyLRUCache {
        private final int cacheSize;
        private final Map<Integer, Node<Integer, Integer>> map;
        private final DoubleLinkedList<Integer, Integer> doubleLinkedList;

        public ZyLRUCache(final int cacheSize) {
            this.cacheSize = cacheSize;
            this.map = new HashMap<>();
            this.doubleLinkedList = new DoubleLinkedList<>();
        }

        public int get(int key) {
            if (!this.map.containsKey(key)) {
                return -1;
            }
            final Node<Integer, Integer> node = this.map.get(key);
            this.doubleLinkedList.removeNode(node);
            this.doubleLinkedList.addHead(node);
            return node.value;
        }

        public void put(int key, int value) {
            if (this.map.containsKey(key)) {
                final Node<Integer, Integer> node = this.map.get(key);
                node.value = value;
                this.map.put(key, node);
                this.doubleLinkedList.removeNode(node);
                this.doubleLinkedList.addHead(node);
            } else {
                if (this.map.size() == this.cacheSize) {
                    final Node<Integer, Integer> lastNode = this.doubleLinkedList.getLast();
                    this.map.remove(lastNode.key);
                    this.doubleLinkedList.removeNode(lastNode);
                }
                final Node<Integer, Integer> newNode = new Node<>(key, value);
                this.map.put(key, newNode);
                this.doubleLinkedList.addHead(newNode);
            }
        }

        public Set<Integer> keySet() {
            return this.map.keySet();
        }
    }

    private static class Node<K, V> {
        private K key;
        private V value;

        private Node<K, V> prev;
        private Node<K, V> next;

        public Node() {
            this.prev = this.next = null;
            this.key = null;
            this.value = null;
        }

        public Node(K key, V value) {
            this();
            this.key = key;
            this.value = value;
        }
    }

    private static class DoubleLinkedList<K, V> {
        private final Node<K, V> head;
        private final Node<K, V> tail;

        public DoubleLinkedList() {
            this.head = new Node<>();
            this.tail = new Node<>();

            this.head.next = this.tail;
            this.tail.prev = this.head;
        }

        /**
         * <pre>
         *      +------+  prev +-----+       +-----+
         * head |      | <---- |     | <---- |     |  tail
         *      +------+       +-----+       +-----+
         * </pre>
         */
        public void addHead(Node<K, V> node) {
            node.next = this.head.next;// 把head后面的对象指向node自己的下一个
            node.prev = this.head;// 把head对象指向node自己的前一个

            this.head.next.prev = node;// 本来head元素挨着的下一个元素是指向head元素的，现在head元素挨着的下一个元素指向node
            this.head.next = node;// head元素的下一个元素指向node
        }

        public void removeNode(Node<K, V> node) {
            node.next.prev = node.prev;
            node.prev.next = node.next;

            node.next = null;
            node.prev = null;
        }

        public Node<K, V> getLast() {
            return this.tail.prev;
        }
    }

}
