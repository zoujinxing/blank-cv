package cn.blank.leetcode;


import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/**
 * 给定一个整数数组 nums 和一个整数目标值 target，请你在该数组中找出 和为目标值 target  的那 两个 整数，并返回它们的数组下标。
 * <br/><a href="https://leetcode.cn/problems/two-sum/">1. 两数之和</a>
 *
 * @author _blank    DateTime 2022/7/28 11:04:38
 */
public class AaTwoSumTests {

    /*
     * Java 实现给定一个数, 获取大于这个数并且最小的2 的幂次方的数
     *
     * HashMap源码里面有一个算法，当程序员构造hashmap如果给定的hashmap容量不是2的n次幂的时候，应该给它一个大于这个数且最小的2的n次幂。
     *
     * 可以参考
     * @see java.util.HashMap#tableSizeFor(int)
     * @see java.util.concurrent.ConcurrentHashMap#tableSizeFor(int)
     */


    public static void main(String[] args) {
        final int[] ints0 = twoSum01(new int[]{2, 7, 11, 15}, 9);
        System.out.println(Arrays.toString(ints0));

        final int[] ints1 = twoSum01(new int[]{3, 8, 12, 16}, 28);
        System.out.println(Arrays.toString(ints1));

        final int[] ints2 = twoSum02(new int[]{3, 2, 4}, 6);
        System.out.println(Arrays.toString(ints2));

        final int[] ints3 = twoSum02(new int[]{4, 3, 5}, 7);
        System.out.println(Arrays.toString(ints3));
    }

    /* 暴力枚举 */
    private static int[] twoSum01(int[] nums, int target) {
        int n = nums.length;
        for (int i = 0; i < n; ++i) {
            for (int j = i + 1; j < n; ++j) {
                if (nums[i] + nums[j] == target) {
                    return new int[]{i, j};
                }
            }
        }
        return new int[0];
    }

    /* 哈希表 */
    private static int[] twoSum02(int[] nums, int target) {
        Map<Integer, Integer> hashtable = new HashMap<>();
        for (int i = 0; i < nums.length; ++i) {
            if (hashtable.containsKey(target - nums[i])) {
                return new int[]{hashtable.get(target - nums[i]), i};
            }
            hashtable.put(nums[i], i);
        }
        return new int[0];
    }

}
