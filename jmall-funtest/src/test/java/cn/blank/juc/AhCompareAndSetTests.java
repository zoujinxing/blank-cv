package cn.blank.juc;

import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicReference;

/**
 * CAS是JDK提供的非阻塞原子性操作，它通过硬件保证了比较-更新的原子性
 */
@Slf4j
public class AhCompareAndSetTests {
    /*
     * CAS是JDK提供的非阻塞原子性操作，它通过硬件保证了比较-更新的原子性。
     * 它是非阻塞的且自身原子性，也就是说这玩意效率更高且通过硬件保证，说明这玩意更可靠。
     *
     * CAS是一条CPU的原子指令（cmpxchg指令），不会造成所谓的数据不一致问题，Unsafe提供的CAS方法（如compareAndSwapXXX）底层实现即为CPU指令cmpxchg。
     * 执行cmpxchg指令的时候，会判断当前系统是否为多核系统，如果是就给总线加锁，只有一个线程会对总线加锁成功，加锁成功之后会执行cas操作，也就是说CAS的原子性实际上是CPU实现的，
     * 其实在这一点上还是有排他锁的，只是比起用synchronized， 这里的排他时间要短的多， 所以在多线程情况下性能会比较好
     */


    /**
     * 演示用，不适合正式使用
     */
    private final AtomicReference<Thread> ar = new AtomicReference<>();

    public void myLock() {
        log.info("进来，加锁~~~");
        final Thread update = Thread.currentThread();
        // 自旋    AtomicReference期望当前是null，更新成当前线程。
        // while (!ar.compareAndSet(null, update)) ;
        for (; ; ) {
            if (ar.compareAndSet(null, update)) {
                break;
            }
        }
    }

    public void myUnLock() {
        final Thread thread = Thread.currentThread();
        ar.compareAndSet(thread, null);
        log.info("出去，解锁~~~");
    }

    public static void main(String[] args) {
        final AhCompareAndSetTests hhCasTests = new AhCompareAndSetTests();

        new Thread(() -> {
            try {
                hhCasTests.myLock();
                log.info("我抢到锁了~~~");
                // 模拟耗时操作
                try {
                    TimeUnit.SECONDS.sleep(5L);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            } finally {
                hhCasTests.myUnLock();
            }
        }, "线程A").start();

        try {
            TimeUnit.SECONDS.sleep(2L);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        new Thread(() -> {
            try {
                hhCasTests.myLock();
                log.info("您好，世界！");
            } finally {
                hhCasTests.myUnLock();
            }
        }, "线程B").start();

    }

}
