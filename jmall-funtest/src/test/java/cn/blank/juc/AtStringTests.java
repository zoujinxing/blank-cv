package cn.blank.juc;

/**
 * 字符串常量在Java内部的加载
 *
 * @author _blank    DateTime 2022/7/28 10:18:46
 */
public class AtStringTests {

    /*
     * String::intern()是一个本地方法，
     * 它的作用是如果字符串常量池中已经包含一个等于此String对象的字符串，
     * 则返回代表池中这个字符串的String对象的引用；
     * 否则，会将此String对象包含的字符串添加 到常量池中，并且返回此String对象的引用。
     */

    /*
     * 周志明老师 2.4.3 方法区和运行时常量池溢出
     * -> 代码清单2-8 String.intern()返回引用的测试
     */

    /*
     * java.lang.System.initializeSystemClass
     * -> sun.misc.Version.init();
     */

    public static void main(String[] args) {
        final String str1 = new StringBuilder("zou").append("jinxing").toString();
        System.out.println(str1);
        System.out.println(str1.intern());
        System.out.println(str1 == str1.intern());

        System.out.println();

        final String str2 = new StringBuilder("ja").append("va").toString();
        System.out.println(str2);
        System.out.println(str2.intern());
        System.out.println(str2 == str2.intern());

        System.out.println();

        final String str3 = new StringBuilder("open").append("jdk").toString();
        System.out.println(str3);
        System.out.println(str3.intern());
        System.out.println(str3 == str3.intern());
    }

}
