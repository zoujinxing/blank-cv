package cn.blank.juc;

import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.StampedLock;
import java.util.function.Consumer;

/**
 * java.util.concurrent.locks.StampedLock
 *
 * @author _blank    DateTime 2022/7/26 13:55:35
 */
@Slf4j
public class AoStampedLockTests {
    /*
     * https://blog.csdn.net/qq_40276626/article/details/119873114
     * https://www.liaoxuefeng.com/wiki/1252599548343744/1309138673991714
     *
     * 前面介绍的ReadWriteLock可以解决多线程同时读，但只有一个线程能写的问题。
     * 如果我们深入分析ReadWriteLock，会发现它有个潜在的问题：如果有线程正在读，写线程需要等待读线程释放锁后才能获取写锁，即读的过程中不允许写，这是一种悲观的读锁。
     * 要进一步提升并发执行效率，Java 8引入了新的读写锁：StampedLock。
     *
     *
     * StampedLock和ReadWriteLock相比，改进之处在于：读的过程中也允许获取写锁后写入！
     * 这样一来，我们读的数据就可能不一致，所以，需要一点额外的代码来判断读的过程中是否有写入，这种读锁是一种乐观锁。
     * 乐观锁的意思就是乐观地估计读的过程中大概率不会有写入，因此被称为乐观锁。
     * 反过来，悲观锁则是读的过程中拒绝有写入，也就是写入必须等待。
     * 显然乐观锁的并发效率更高，但一旦有小概率的写入导致读取的数据不一致，需要能检测出来，再读一遍就行。
     *
     *
     * StampedLock提供了乐观读锁，可取代ReadWriteLock以进一步提升并发性能；
     * StampedLock是不可重入锁。
     *
     *
     * StampedLock是基于CLH锁原理实现的,
     * CLH是一种基于排队思想实现的自旋锁，可以保证FIFO(先进先出)的服务顺序，
     * 所以会避免写线程饥饿问题，其实就是其中实现了一个队列,
     * 每次不管是读锁也好写锁也好,未拿到锁就加入队列,然后每次解锁后队列头存储的线程节点获取锁,以此避免饥饿。
     */

    public static void main(String[] args) {
        example00();
    }

    // 悲观读
    private static void reading(final Consumer<String> readThread, final ExecutorService threadPool, final CountDownLatch countDownLatch) {
        threadPool.submit(() -> {
            Thread.currentThread().setName(" read-thread");

            try {
                readThread.accept(" read");
            } finally {
                countDownLatch.countDown();
            }
        });
        try {
            TimeUnit.SECONDS.sleep(1L);
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
            log.error(e.getMessage(), e);
        }
    }

    // 乐观读
    private static void optimisticReading(final Consumer<String> optimisticReadThread, final ExecutorService threadPool, final CountDownLatch countDownLatch) {
        threadPool.submit(() -> {
            Thread.currentThread().setName("optimistic-read-thread");

            try {
                optimisticReadThread.accept("Optimistic Reading");
            } finally {
                countDownLatch.countDown();
            }
        });
        try {
            TimeUnit.SECONDS.sleep(2L);
//            TimeUnit.SECONDS.sleep(6L);
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
            log.error(e.getMessage(), e);
        }
    }

    private static void example00() {
//        final LongAccumulator number = new LongAccumulator(Long::sum, 37L);
        final AtomicInteger number = new AtomicInteger(37);
        final StampedLock stampedLock = new StampedLock();

        final Consumer<String> writeThread = x -> {
            final long writeLockStamp = stampedLock.writeLock();

            log.info("写线程准备修改数据！");
            try {
                number.set(Integer.sum(number.get(), 13));
            } finally {
                stampedLock.unlockWrite(writeLockStamp);
            }
            log.info("写线程结束修改数据！");
        };

        final Consumer<String> readThread = x -> {
            final long readLockStamp = stampedLock.readLock();

            try {
                final int second = 4;
                log.info("进入读锁代码块，{}秒后跳出。。。", 4);
                for (int i = 0; i < second; i++) {
                    log.info("正在读取中。。。{}", i + 1);
                    try {
                        TimeUnit.SECONDS.sleep(1L);
                    } catch (InterruptedException e) {
                        Thread.currentThread().interrupt();
                        log.error(e.getMessage(), e);
                    }
                }

                final int result = number.get();
                log.info("获得成员变量值 result: {}，    写线程没有修改成功，读锁时候写锁无法介入，传统的读写互斥！", result);
            } finally {
                stampedLock.unlockRead(readLockStamp);
            }
        };

        // 乐观读
        final Consumer<String> optimisticReadThread = x -> {
            final long tryOptimisticReadStamp = stampedLock.tryOptimisticRead();

            int result = number.get();
            log.info("4秒前stampedLock.validate方法值（true无修改，false有修改）：{}", stampedLock.validate(tryOptimisticReadStamp));

            final int second = 4;
            for (int i = 0; i < second; i++) {
                log.info("正在读取中 {} 秒后 stampedLock.validate 方法值（true无修改，false有修改）：{}", i + 1, stampedLock.validate(tryOptimisticReadStamp));
                try {
                    TimeUnit.SECONDS.sleep(1L);
                } catch (InterruptedException e) {
                    Thread.currentThread().interrupt();
                    log.error(e.getMessage(), e);
                }
            }

            if (!stampedLock.validate(tryOptimisticReadStamp)) {
                log.info("成员变量值 result 有被修改过（有写操作）");

                final long readLockStamp = stampedLock.readLock();
                try {
                    log.info("从 乐观读 升级为 悲观读 ！");
                    result = number.get();
                    log.info("悲观读后成员变量值 result: {}", result);
                } finally {
                    stampedLock.unlockRead(readLockStamp);
                }
            }

            log.info("最终成员变量值 result: {}", result);
        };

        final ExecutorService threadPool = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors());
        final CountDownLatch countDownLatch = new CountDownLatch(2);

//        reading(readThread, threadPool, countDownLatch);// 2选1
        optimisticReading(optimisticReadThread, threadPool, countDownLatch);// 2选1
        threadPool.submit(() -> {
            Thread.currentThread().setName("write-thread");

            log.info("进入写线程~~~");
            try {
                writeThread.accept("write");
            } finally {
                countDownLatch.countDown();
            }
        });

        try {
            countDownLatch.await();
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
            log.error(e.getMessage(), e);
        }

        log.info("finally 主线程获取 result: {}", number.get());

        threadPool.shutdown();
    }

}
