package cn.blank.juc;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.LockSupport;

/**
 * LockSupport是用来创建锁和其他同步类的基本线程阻塞原语
 */
public class AeLockSupportTests {

    public static void main(String[] args) {
        final Thread thread = new Thread(() -> {
            System.out.println("进来先");
            LockSupport.park();
            System.out.println("您好，世界！");
        });
        thread.start();

        try {
            TimeUnit.SECONDS.sleep(2L);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("两秒后打印");
        LockSupport.unpark(thread);

    }

}