package cn.blank.juc;

import java.util.concurrent.TimeUnit;

/**
 * 死锁演示
 *
 * @author _blank    DateTime 2022/7/28 1:03:48
 */
public class AsDeadLockTests {

    private static class DeadLock implements Runnable {
        private final String objLockAb;
        private final String objLockAa;

        DeadLock(final String objLockAa, final String objLockAb) {
            this.objLockAa = objLockAa;
            this.objLockAb = objLockAb;
        }

        @Override
        public void run() {
            synchronized (this.objLockAa) {
                System.out.println(Thread.currentThread().getName() + "    自己持有：" + this.objLockAa + "    尝试获得：" + this.objLockAb);
                try {
                    TimeUnit.SECONDS.sleep(2L);
                } catch (InterruptedException e) {
                    Thread.currentThread().interrupt();
                    e.printStackTrace();
                }
                synchronized (this.objLockAb) {
                    System.out.println(Thread.currentThread().getName() + "    自己持有：" + this.objLockAb + "    尝试获得：" + this.objLockAa);
                }
            }
        }
    }

    /*
     *
     * 1、top                     命令，然后按shift+p按照CPU排序,找到占用CPU过高的进程的pid
     * 2、top -H -p [进程id]      找到进程中消耗资源最高的线程的id
     * 3、printf "%x\n" [线程id]  将需要的线程ID转换为16进制格式
     * 4、jstack [进程id] | grep -A 10 [线程id的16进制]”
     *
     *
     * shell 命令查死锁 jstack pid
     */
    public static void main(String[] args) {
        final String aa = "AA";
        final String bb = "BB";
        new Thread(new DeadLock(aa, bb), "thread-AA").start();
        new Thread(new DeadLock(bb, aa), "thread-BB").start();
    }

}
