package cn.blank.juc;

/**
 * 线程中断
 */
public class AdThreadInterruptTests {

    public static void main(String[] args) {

        Thread.currentThread().interrupt();

        final boolean interrupted = Thread.currentThread().isInterrupted();

        final boolean staticInterrupted = Thread.interrupted();

    }

}