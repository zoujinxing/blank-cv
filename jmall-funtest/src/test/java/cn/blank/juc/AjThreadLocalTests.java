package cn.blank.juc;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;

import java.lang.ref.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;

/**
 * ThreadLocal提供线程局部变量。
 * 这些变量与正常的变量不同，因为每一个线程在访问ThreadLocal实例的时候（通过其get或set方法）都有自己的、独立初始化的变量副本。
 * ThreadLocal实例通常是类中的私有静态字段，使用它的目的是希望将状态（例如，用户ID或事务ID）与线程关联起来。
 */
@Slf4j
public class AjThreadLocalTests {

    /* org.springframework.core.NamedThreadLocal */
    private static final ThreadLocal<SimpleDateFormat> SIMPLE_DATE_FORMAT_THREAD_LOCAL = ThreadLocal.withInitial(() -> new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS"));

    @Data
    @AllArgsConstructor
    @Slf4j
    private static class PersonReference {
        private String name;

        @Override
        protected void finalize() throws Throwable {
            super.finalize();
            log.info("我即将被回收了！");
        }
    }

    public static void main(String[] args) throws InterruptedException {
        // 虚引用
        final ReferenceQueue<PersonReference> referenceQueue = new ReferenceQueue<>();
        final PhantomReference<PersonReference> phantomReference = new PhantomReference<>(new PersonReference("赵六"), referenceQueue);

        @SuppressWarnings("MismatchedReadAndWriteOfArray") final byte[][] ref = new byte[1024][];

        new Thread(() -> {
            for (int i = 0; i < ref.length; i++) {
                ref[i] = new byte[128 * 1024 * 1024];
                try {
                    TimeUnit.MILLISECONDS.sleep(500L);
                } catch (InterruptedException e) {
                    log.error(e.getMessage(), e);
                }
                log.info("休眠，{}，{}", i, phantomReference.get());
            }
        }, "抢内存线程").start();

        new Thread(() -> {
            for (; ; ) {
                // 当堆内存不足时候，触发了自动gc，此处才会调用
                final Reference<? extends PersonReference> reference = referenceQueue.poll();
                if (reference != null) {
                    log.info("幽灵队列已有数据过来~");
                    break;
                }
            }
        }, "取幽灵队列数据线程").start();
    }

    private static void example4() throws InterruptedException {
        // 弱引用
        final WeakReference<PersonReference> weakReference = new WeakReference<>(new PersonReference("王五"));

        log.info("内存够用，还未开始gc, {}", weakReference.get());

        System.gc();
        TimeUnit.SECONDS.sleep(1L);

        log.info("内存够用，已经手动触发过gc, {}", weakReference.get());
    }

    private static void example3() throws InterruptedException {
        // 软引用
        final SoftReference<PersonReference> softReference = new SoftReference<>(new PersonReference("李四"));

        System.gc();
        TimeUnit.SECONDS.sleep(1L);
        log.info("内存够用下：{}", softReference.get());
        @SuppressWarnings("MismatchedReadAndWriteOfArray") final byte[][] ref = new byte[32][];
        try {
            for (int i = 0; i < 32; i++) {
                ref[i] = new byte[1024 * 1024 * 1024];
                TimeUnit.MILLISECONDS.sleep(1000L);
            }
        } finally {
            log.info("内存不够用情况下:{}", softReference.get());
        }
    }

    private static void example2() throws InterruptedException {
        // 强引用
        PersonReference strongReference = new PersonReference("张三");
        log.info("gc前：{}", strongReference);
        System.gc();
        TimeUnit.SECONDS.sleep(1L);
        log.info("gc后：{}", strongReference);
    }

    private static void example1() {
        for (int i = 0; i < Runtime.getRuntime().availableProcessors(); i++) {
            final int randomNum = i;
            new Thread(() -> {
                final Date parseDate;
                try {
                    parseDate = SIMPLE_DATE_FORMAT_THREAD_LOCAL.get().parse("2012-12-12 22:22:2" + randomNum + ".26" + randomNum);
                    log.info("{}", parseDate);
                } catch (ParseException e) {
                    log.error(e.getMessage(), e);
                } finally {
                    SIMPLE_DATE_FORMAT_THREAD_LOCAL.remove();
                }
            }, "第" + i + "个线程").start();
        }
    }

}