package cn.blank.juc;

import java.util.*;
import java.util.concurrent.*;

/**
 * Java 集合框架
 *
 * @author _blank    DateTime 2022/7/27 14:59:33
 */
@SuppressWarnings({"UnusedAssignment", "unused"})
public class AqContainerTests {

    static {
        // -------------------------------------------------------------------------------------------------------------
        java.util.Collection<Object> collection;
        // -------------------------------------------------------------------------------------------------------------


        // -------------------------------------------------------------------------------------------------------------
        java.util.List<Object> list;// 有序集合，元素可以重复
        // -------------------------------------------------------------------------------------------------------------
        list = new ArrayList<>();//                          https://so.toutiao.com/search?dvpf=pc&pd=information&keyword=Java%20ArrayList
        list = new LinkedList<>();//                         https://so.toutiao.com/search?dvpf=pc&pd=information&keyword=Java%20LinkedList
        list = new Stack<>();// 栈，last-in-first-out (LIFO)  https://so.toutiao.com/search?dvpf=pc&pd=information&keyword=Java%20Stack
        list = new Vector<>();//                             https://so.toutiao.com/search?dvpf=pc&pd=information&keyword=Java%20Vector
        list = new CopyOnWriteArrayList<>();// JUC           https://so.toutiao.com/search?dvpf=pc&pd=information&keyword=Java%20CopyOnWriteArrayList


        // -------------------------------------------------------------------------------------------------------------
        java.util.Queue<?> queue;// 队列，FIFO (first-in-first-out)
        // -------------------------------------------------------------------------------------------------------------
        queue = new PriorityQueue<>();//                      https://so.toutiao.com/search?dvpf=pc&pd=information&keyword=Java%20PriorityQueue
        queue = new ArrayBlockingQueue<>(16);// JUC   https://so.toutiao.com/search?dvpf=pc&pd=information&keyword=Java%20ArrayBlockingQueue
        queue = new ConcurrentLinkedQueue<>();// JUC          https://so.toutiao.com/search?dvpf=pc&pd=information&keyword=Java%20ConcurrentLinkedQueue
        queue = new DelayQueue<>();// JUC                     https://so.toutiao.com/search?dvpf=pc&pd=information&keyword=Java%20DelayQueue
        queue = new LinkedBlockingQueue<>();// JUC            https://so.toutiao.com/search?dvpf=pc&pd=information&keyword=Java%20LinkedBlockingQueue
        queue = new LinkedTransferQueue<>();// JUC            https://so.toutiao.com/search?dvpf=pc&pd=information&keyword=Java%20LinkedTransferQueue
        queue = new PriorityBlockingQueue<>();// JUC          https://so.toutiao.com/search?dvpf=pc&pd=information&keyword=Java%20PriorityBlockingQueue
        queue = new SynchronousQueue<>();// JUC               https://so.toutiao.com/search?dvpf=pc&pd=information&keyword=Java%20SynchronousQueue


        // -------------------------------------------------------------------------------------------------------------
        java.util.Deque<Object> deque;// 双端队列
        // -------------------------------------------------------------------------------------------------------------
        deque = new ArrayDeque<>();//                       https://so.toutiao.com/search?dvpf=pc&pd=information&keyword=Java%20ArrayDeque
        deque = new ConcurrentLinkedDeque<>();// JUC        https://so.toutiao.com/search?dvpf=pc&pd=information&keyword=Java%20ConcurrentLinkedDeque
        deque = new LinkedBlockingDeque<>();// JUC          https://so.toutiao.com/search?dvpf=pc&pd=information&keyword=Java%20LinkedBlockingDeque


        // -------------------------------------------------------------------------------------------------------------
        java.util.Set<Object> set;// 无序集合，元素不可重复
        // -------------------------------------------------------------------------------------------------------------
        set = new HashSet<>();//                    https://so.toutiao.com/search?dvpf=pc&pd=information&keyword=Java%20HashSet
        set = new LinkedHashSet<>();//              https://so.toutiao.com/search?dvpf=pc&pd=information&keyword=Java%20LinkedHashSet
        set = new TreeSet<>();//                    https://so.toutiao.com/search?dvpf=pc&pd=information&keyword=Java%20TreeSet
        set = new ConcurrentSkipListSet<>();// JUC  https://so.toutiao.com/search?dvpf=pc&pd=information&keyword=Java%20ConcurrentSkipListSet
        set = new CopyOnWriteArraySet<>();// JUC    https://so.toutiao.com/search?dvpf=pc&pd=information&keyword=Java%20CopyOnWriteArraySet


        // -------------------------------------------------------------------------------------------------------------
        java.util.Map<?, Object> map;// 存储一组键值对象的集合
        // -------------------------------------------------------------------------------------------------------------
        map = new EnumMap<>(TimeUnit.class);//        https://so.toutiao.com/search?dvpf=pc&pd=information&keyword=Java%20EnumMap
        map = new HashMap<>();//                      https://so.toutiao.com/search?dvpf=pc&pd=information&keyword=Java%20HashMap
        map = new IdentityHashMap<>();//              https://so.toutiao.com/search?dvpf=pc&pd=information&keyword=Java%20IdentityHashMap
        map = new LinkedHashMap<>();//                https://so.toutiao.com/search?dvpf=pc&pd=information&keyword=Java%20LinkedHashMap
        map = new TreeMap<>();//                      https://so.toutiao.com/search?dvpf=pc&pd=information&keyword=Java%20TreeMap
        map = new WeakHashMap<>();//                  https://so.toutiao.com/search?dvpf=pc&pd=information&keyword=Java%20WeakHashMap
        map = new ConcurrentHashMap<>();// JUC        https://so.toutiao.com/search?dvpf=pc&pd=information&keyword=Java%20ConcurrentHashMap
        map = new ConcurrentSkipListMap<>();// JUC    https://so.toutiao.com/search?dvpf=pc&pd=information&keyword=Java%20ConcurrentSkipListMap


        // -------------------------------------------------------------------------------------------------------------
        java.util.Dictionary<Object, Object> dictionary;// 用来存储键/值对，作用和Map类相似
        // -------------------------------------------------------------------------------------------------------------
        dictionary = new Hashtable<>();//             https://so.toutiao.com/search?dvpf=pc&pd=information&keyword=Java%20Hashtable
        dictionary = new Properties();//              https://so.toutiao.com/search?dvpf=pc&pd=information&keyword=Java%20Properties
    }


    static {
        // 工具类
        final Class<Arrays> arraysClass = Arrays.class;
        final Class<Collections> collectionsClass = Collections.class;
    }


    /*
     * 1、故障现象
     *     java.util.ConcurrentModificationException
     * 2、导致原因
     *
     * 3、解决方案
     *    1> new java.util.Vector<>();
     *    2> java.util.Collections.synchronizedList(new java.util.ArrayList<>());
     *    3> new java.util.concurrent.CopyOnWriteArrayList<>();
     *           java.util.concurrent.CopyOnWriteArraySet
     *           java.util.concurrent.ConcurrentHashMap
     *
     * 4、优化建议
     *     CopyOnWrite容器即写时复制的容器。
     *     通俗的理解是当我们往一个容器添加元素的时候，不直接往当前容器添加，而是先将当前容器进行Copy，复制出一个新的容器，然后新的容器里添加元素，添加完元素之后，再将原容器的引用指向新的容器。
     *     这样做的好处是我们可以对CopyOnWrite容器进行并发的读，而不需要加锁，因为当前容器不会添加任何元素。
     *     所以CopyOnWrite容器也是一种读写分离的思想，读和写不同的容器。
     */

    public static void main(String[] args) {
//        example00();
//        example01();
//        example02();
        example03();
        try {
            TimeUnit.SECONDS.sleep(3L);
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
            e.printStackTrace();
        }
    }

    private static void example00() {
        final List<String> list = new ArrayList<>();// CopyOnWriteArrayList

        for (int i = 0; i < 25; i++) {
            new Thread(() -> {
                list.add(UUID.randomUUID().toString().substring(0, 8));
                System.out.println(list);
            }, "thread-list-" + i).start();
        }
    }

    private static void example01() {
        final Set<Object> set = new HashSet<>();// CopyOnWriteArraySet
        for (int i = 0; i < 25; i++) {
            new Thread(() -> {
                set.add(UUID.randomUUID().toString().substring(0, 8));
                System.out.println(set);
            }, "thread-set-" + i).start();
        }
    }

    private static void example02() {
        final Map<String, String> map = new HashMap<>();// ConcurrentHashMap
        for (int i = 0; i < 25; i++) {
            new Thread(() -> {
                map.put(Thread.currentThread().getName(), UUID.randomUUID().toString().substring(0, 8));
                System.out.println(map);
            }, "thread-map-" + i).start();
        }
    }

    private static void example03() {
        final Map<String, Integer> scores = new ConcurrentHashMap<>();
        scores.put("张三", 0);

        final Runnable runnable = () -> {
            for (int i = 0; i < 1000; i++) {
                final Integer zhangSan = scores.get("张三");
                final int sum = Integer.sum(zhangSan, 1);
                scores.put("张三", sum);
            }
        };
        final Runnable upgradedVersion = () -> {
            for (int i = 0; i < 1000; i++) {
                synchronized (AqContainerTests.class) {
                    final Integer zhangSan = scores.get("张三");
                    final int sum = Integer.sum(zhangSan, 1);
                    scores.put("张三", sum);
                }
            }
        };
        final Runnable finalVersion = () -> {
            for (int i = 0; i < 1000; i++) {
                while (true) {
                    final Integer oldValue = scores.get("张三");
                    if (scores.replace("张三", oldValue, Integer.sum(oldValue, 1))) {
                        break;
                    }
                }
            }
        };

        final Thread chinese = new Thread(runnable, "语文");
        final Thread mathematics = new Thread(runnable, "数学");

        chinese.start();
        mathematics.start();

        try {
            chinese.join();
            mathematics.join();
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
            e.printStackTrace();
        }

        System.out.println(scores);
    }

}
