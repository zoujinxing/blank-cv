package cn.blank.juc;

import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.Semaphore;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

/**
 * 抽象的队列同步器
 */
@Slf4j
public class AmAbstractQueuedSynchronizerTests {

    /*
     * @see java.util.concurrent.locks.AbstractQueuedSynchronizer
     * @see java.util.concurrent.locks.AbstractQueuedSynchronizer#state    同步状态成员变量
     * @see java.util.concurrent.locks.AbstractQueuedSynchronizer.Node    CLH等待队列（双向队列）
     */

    public static void main(String[] args) {

        final Lock lockFair = new ReentrantLock(true);// 公平锁

        final Lock lockNonFair = new ReentrantLock(false);// 非公平锁


        final Semaphore semaphore = new Semaphore(1);


        final CyclicBarrier cyclicBarrier = new CyclicBarrier(1);


        // https://blog.csdn.net/qq_39241239/article/details/87030142
        final CountDownLatch countDownLatch = new CountDownLatch(1);


        final ReadWriteLock reentrantReadWriteFairLock = new ReentrantReadWriteLock(true);// 读写公平锁

        final ReadWriteLock reentrantReadWriteNonFairLock = new ReentrantReadWriteLock(false);// 读写非公平锁


        // 在一个线程中
        try {
            lockFair.lock();
        } finally {
            lockFair.unlock();
        }

        // 在一个线程中
        try {
            lockNonFair.lock();
        } finally {
            lockNonFair.unlock();
        }

        // 在一个线程中
        try {
            semaphore.acquire();
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
            log.error(e.getMessage(), e);
        } finally {
            semaphore.release();
        }

        // 在一个线程中
        try {
            cyclicBarrier.await();
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
            log.error(e.getMessage(), e);
        } catch (BrokenBarrierException e) {
            log.error(e.getMessage(), e);
        }

        // 循环栅栏
        cyclicBarrier.reset();


        // 在一个线程中
        try {
            System.out.println("do something...");
        } finally {
            countDownLatch.countDown();
        }

        try {
            countDownLatch.await();
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
            log.error(e.getMessage(), e);
        }

        // 在一个线程中
        try {
            reentrantReadWriteFairLock.readLock().lock();
        } finally {
            reentrantReadWriteFairLock.readLock().unlock();
        }

        // 在一个线程中
        try {
            reentrantReadWriteFairLock.writeLock().lock();
        } finally {
            reentrantReadWriteFairLock.writeLock().unlock();
        }

        // 在一个线程中
        try {
            reentrantReadWriteNonFairLock.readLock().lock();
        } finally {
            reentrantReadWriteNonFairLock.readLock().unlock();
        }

        // 在一个线程中
        try {
            reentrantReadWriteNonFairLock.writeLock().lock();
        } finally {
            reentrantReadWriteNonFairLock.writeLock().unlock();
        }
    }

}