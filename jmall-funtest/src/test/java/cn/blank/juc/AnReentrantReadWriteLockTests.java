package cn.blank.juc;

import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

/**
 * 读写锁
 * 一个资源能够被多个读线程访问，或者被一个写线程访问，但是不能同时存在读写线程。
 * java.util.concurrent.locks.ReentrantReadWriteLock
 */
@Slf4j
public class AnReentrantReadWriteLockTests {

    /* 读多写少场景 */

    public static void main(String[] args) {
        example00();
        example01();
        example02();
    }

    /* 读写锁降级；读锁不可以升级写锁 */
    private static void example00() {
        // 锁降级 (获取写锁的时候，可以获取读锁，反之则不行)
        final ReentrantReadWriteLock readWriteLock = new ReentrantReadWriteLock(false);

        // 重入还允许从写锁降级到读锁，方法是获取写锁，然后读锁，然后释放写锁。
        try {
            readWriteLock.writeLock().lock();
            System.out.println("获得写锁，正在写入。。。");

            try {
                readWriteLock.readLock().lock();
                System.out.println("获得读锁，正在读取。。。");
            } finally {
                readWriteLock.readLock().unlock();
            }
        } finally {
            readWriteLock.writeLock().unlock();
        }

        System.out.println("end...");

        // 反例，不可以的操作，锁升级不被允许！（但是，从读锁升级到写锁是不可能的。）
//        try {
//            readWriteLock.readLock().lock();
//            System.out.println("获得读锁，正在读取。。。");
//
//            try {
//                readWriteLock.writeLock().lock();
//                System.out.println("获得写锁，正在写入。。。");
//            } finally {
//                readWriteLock.writeLock().unlock();
//            }
//        } finally {
//            readWriteLock.readLock().unlock();
//        }
    }

    /* 写互斥，读共享 */
    private static void example01() {
        final Lock reentrantLock = new ReentrantLock();

        for (int i = 0; i < 10; i++) {
            new Thread(() -> {
                try {
                    reentrantLock.lock();
                    System.out.println(Thread.currentThread().getName() + "    " + "正在写入。。。");
                    TimeUnit.MILLISECONDS.sleep(500L);
                    System.out.println(Thread.currentThread().getName() + "    " + "完成写入。。。");
                } catch (InterruptedException e) {
                    Thread.currentThread().interrupt();
                    log.error(e.getMessage(), e);
                } finally {
                    reentrantLock.unlock();
                }
            }, "write thread " + i).start();
        }

        for (int i = 0; i < 10; i++) {
            new Thread(() -> {
                try {
                    reentrantLock.lock();
                    System.out.println(Thread.currentThread().getName() + "    " + "正在读取。。。");
                    TimeUnit.MILLISECONDS.sleep(200L);
                    System.out.println(Thread.currentThread().getName() + "    " + "完成读取。。。");
                } catch (InterruptedException e) {
                    Thread.currentThread().interrupt();
                    log.error(e.getMessage(), e);
                } finally {
                    reentrantLock.unlock();
                }
            }, "read thread " + i).start();
        }
    }

    /* 读写互斥 */
    private static void example02() {
        final ReadWriteLock readWriteLock = new ReentrantReadWriteLock();

        for (int i = 0; i < 10; i++) {
            new Thread(() -> {
                try {
                    readWriteLock.writeLock().lock();
                    System.out.println(Thread.currentThread().getName() + "    " + "正在写入。。。");
                    TimeUnit.MILLISECONDS.sleep(500L);
                    System.out.println(Thread.currentThread().getName() + "    " + "完成写入。。。");
                } catch (InterruptedException e) {
                    Thread.currentThread().interrupt();
                    log.error(e.getMessage(), e);
                } finally {
                    readWriteLock.writeLock().unlock();
                }
            }, "write thread " + i).start();
        }

        for (int i = 0; i < 10; i++) {
            new Thread(() -> {
                try {
                    readWriteLock.readLock().lock();
                    System.out.println(Thread.currentThread().getName() + "    " + "正在读取。。。");
                    TimeUnit.SECONDS.sleep(2L);
                    System.out.println(Thread.currentThread().getName() + "    " + "完成读取。。。");
                } catch (InterruptedException e) {
                    Thread.currentThread().interrupt();
                    log.error(e.getMessage(), e);
                } finally {
                    readWriteLock.readLock().unlock();
                }
            }, "read thread " + i).start();
        }

        try {
            TimeUnit.SECONDS.sleep(1L);
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
            log.error(e.getMessage(), e);
        }

        for (int i = 0; i < 3; i++) {
            new Thread(() -> {
                try {
                    readWriteLock.writeLock().lock();
                    System.out.println(Thread.currentThread().getName() + "    " + "正在写入。。。");
                    TimeUnit.MILLISECONDS.sleep(500L);
                    System.out.println(Thread.currentThread().getName() + "    " + "完成写入。。。");
                } catch (InterruptedException e) {
                    Thread.currentThread().interrupt();
                    log.error(e.getMessage(), e);
                } finally {
                    readWriteLock.writeLock().unlock();
                }
            }, "new write thread " + i).start();
        }
    }

}