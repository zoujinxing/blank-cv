package cn.blank.juc;

import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Java 线程池
 *
 * @author _blank    DateTime 2022/7/27 17:36:17
 */
public class ArThreadPoolTests {

    public static void main(String[] args) {
        example00();
    }

    private static volatile boolean FLAG = true;

    /* 线程通信之生产者消费者阻塞队列版 */
    private static void example00() {
        final AtomicInteger atomicInteger = new AtomicInteger();
        final BlockingQueue<String> blockingQueue = new ArrayBlockingQueue<>(16);

        final Runnable producer = () -> {
            String data;
            boolean returnValue = false;
            while (FLAG) {
                data = String.valueOf(atomicInteger.incrementAndGet());
                try {
                    returnValue = blockingQueue.offer(data, 2L, TimeUnit.SECONDS);
                } catch (InterruptedException e) {
                    Thread.currentThread().interrupt();
                    e.printStackTrace();
                }
                if (returnValue) {
                    System.out.println(Thread.currentThread().getName() + "    插入队列 " + data + " 成功");
                } else {
                    System.out.println(Thread.currentThread().getName() + "    插入队列 " + data + " 失败");
                }
                try {
                    TimeUnit.SECONDS.sleep(1L);
                } catch (InterruptedException e) {
                    Thread.currentThread().interrupt();
                    e.printStackTrace();
                }
            }
            System.out.println(Thread.currentThread().getName() + "    生产者停止了生产");
        };

        final Runnable consumer = () -> {
            String result = null;
            while (FLAG) {
                try {
                    result = blockingQueue.poll(2L, TimeUnit.SECONDS);
                } catch (InterruptedException e) {
                    Thread.currentThread().interrupt();
                    e.printStackTrace();
                }
                if (null == result || "".equals(result)) {
                    FLAG = false;
                    System.out.println(Thread.currentThread().getName() + "超过2秒钟没有取到生产者发来的数据，消费者退出\n\n");
                    return;
                }
                System.out.println(Thread.currentThread().getName() + "    消费队列 " + result + " 成功");
            }
        };

        new Thread(() -> {
            System.out.println(Thread.currentThread().getName() + "    生产者线程启动");
            producer.run();
        }, "producer").start();
        new Thread(() -> {
            System.out.println(Thread.currentThread().getName() + "    消费者线程启动\n\n");
            consumer.run();
        }, "consumer").start();

        try {
            TimeUnit.SECONDS.sleep(5L);
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
            e.printStackTrace();
        }
        System.out.println("\n\n5秒时间到，停止任务");
        FLAG = false;
    }

    /*
     * cpu密集型    Runtime.getRuntime().availableProcessors()
     *
     * io密集型    (1) Runtime.getRuntime().availableProcessors() * 2    (2) CPU核数 / 1 - 阻塞系数（阻塞系数在0.8~0.9之间）
     */

    private static void example01() {
        // 线程池中的常驻核心线程数
        final int corePoolSize = 4;
        // 线程池能够容纳同时执行的最大线程数，此值必须大于等于1
        // 注意，当核心线程数处理满了，任务队列也满了，新进来的任务将会优先被maximumPoolSize多出的线程执行，而不是先从任务队列取任务
        final int maximumPoolSize = 8;
        // 多余的空闲线程的存活时间。当前线程池数量超过corePoolSize时，当空闲时间达到KeepAliveTime值时，多余空闲线程会被销毁直到只剩下corePoolSize个线程为止
        final long keepAliveTime = 60L;
        // KeepAliveTime的单位
        final TimeUnit unit = TimeUnit.SECONDS;
        // 任务队列，被提交但尚未被执行的任务
        final BlockingQueue<Runnable> workQueue = new ArrayBlockingQueue<>(1024);
        // 表示生成线程池中工作线程的线程工厂，用于创建线程一般用默认的即可
        final ThreadFactory threadFactory = Executors.defaultThreadFactory();
        // 拒绝策略，表示当队列满了并且工作线程大于等于线程池的最大线程数（maximumPoolSize）时如何来拒绝来请求的Runnable的策略
        RejectedExecutionHandler handler;
        // 直接抛出RejectedExecutionException异常阻止系统正常运行
        handler = new ThreadPoolExecutor.AbortPolicy();
        // "调用者运行"一种调节机制，该策略既不会抛弃任务，也不会抛出异常，而是将某些任务回退到调用者，从而降低新任务的流量。
        handler = new ThreadPoolExecutor.CallerRunsPolicy();
        // 抛弃队列中等待最久的任务，然后把当前任务加入到队列中尝试再次提交当前任务。
        handler = new ThreadPoolExecutor.DiscardOldestPolicy();
        // 直接丢弃任务，不予任何处理也不抛出异常。如果允许任务丢失，这是最好的一种方案。
        handler = new ThreadPoolExecutor.DiscardPolicy();

        /* 线程池 */
        final ThreadPoolExecutor threadPoolExecutor = new ThreadPoolExecutor(
                corePoolSize
                , maximumPoolSize
                , keepAliveTime
                , unit
                , workQueue
                , threadFactory
                , handler
        );

        /* 定时任务线程池 */
        final ScheduledThreadPoolExecutor scheduledThreadPoolExecutor = new ScheduledThreadPoolExecutor(
                corePoolSize
                , threadFactory
                , handler
        );

        Runnable command = () -> System.out.println("hello world");
        long initialDelay = 1000L;
        long period = 3L;// 重复任务的周期。值 0 表示非重复任务。正值表示固定利率执行。
        long delay = 5L;// 重复任务的周期。值 0 表示非重复任务。负值表示固定延迟执行。
        scheduledThreadPoolExecutor.scheduleAtFixedRate(command, initialDelay, period, unit);
        scheduledThreadPoolExecutor.scheduleWithFixedDelay(command, initialDelay, delay, unit);
    }

    private static void example02() {
        // 一池4个线程
        Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors());

        // 一池1个线程
        Executors.newSingleThreadExecutor(Executors.defaultThreadFactory());

        // 一池N个线程
        Executors.newCachedThreadPool(Executors.defaultThreadFactory());

        Executors.newWorkStealingPool(Runtime.getRuntime().availableProcessors());

        Executors.newScheduledThreadPool(Runtime.getRuntime().availableProcessors());

        Executors.newSingleThreadScheduledExecutor(Executors.defaultThreadFactory());

    }

}
