package cn.blank.juc;

import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.TimeUnit;

/**
 * 用户线程和守护线程
 */
@Slf4j
public class AaThreadDaemonTests {

    public static void main(String[] args) {
        Thread thread = new Thread(() -> {
            while (true) {
                try {
                    TimeUnit.MILLISECONDS.sleep(100L);
                } catch (InterruptedException e) {
                    log.error(e.getMessage(), e);
                }
                log.info("您好，世界！ {}", System.currentTimeMillis());
            }
        });
        thread.setDaemon(true);
        thread.start();

        try {
            TimeUnit.SECONDS.sleep(3);
            log.info("主线程即将退出，守护线程也自动退出！");
        } catch (InterruptedException e) {
            log.error(e.getMessage(), e);
        }
    }

}