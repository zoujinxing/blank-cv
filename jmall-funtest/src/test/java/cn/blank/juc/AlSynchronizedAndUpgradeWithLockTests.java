package cn.blank.juc;

import org.openjdk.jol.info.ClassLayout;
import org.openjdk.jol.vm.VM;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

/**
 * Synchronized 与 锁升级
 */
public class AlSynchronizedAndUpgradeWithLockTests {
    private static final String[] ZERO_RUNS;

    static {
        ZERO_RUNS = new String[64];
        String s = "";
        for (int c = 0; c < ZERO_RUNS.length; c++) {
            ZERO_RUNS[c] = s;
            s += "0";
        }
    }

    private static String toHexString(long x) {
        String s = Long.toHexString(x);
        int deficit = 16 - s.length();
        String ack = ZERO_RUNS[deficit] + s;
        final StringBuilder pw = new StringBuilder(88);
        for (int i = 0; i < ack.length(); i++) {
            pw.append("    ").append(ack, i, i + 1);
        }
        return "0x" + pw;
    }

    private static String toBinaryString(long x) {
        String s = Long.toBinaryString(x);
        int deficit = 64 - s.length();
        String ack = ZERO_RUNS[deficit] + s;
        final StringBuilder pw = new StringBuilder(88);
        for (int i = 0; i < ack.length() / 4; i++) {
            pw.append(" ").append(ack, i * 4, i * 4 + 4);
        }

        return "0b" + pw;
    }

    private static String toHex(long x) {
        String s = Long.toHexString(x);
        int deficit = 16 - s.length();
        return "0x" + ZERO_RUNS[deficit] + s;
    }

    private static String parseMarkWord(long mark) {
        //  64 bits:
        //  unused:25 hash:31 -->| unused_gap:1   age:4    biased_lock:1 lock:2 (normal object)
        //  JavaThread*:54 epoch:2 unused_gap:1   age:4    biased_lock:1 lock:2 (biased object)
        long bits = mark & 0b11;
        switch ((int) bits) {
            case 0b11:
                return "(marked: " + toHex(mark) + ")";
            case 0b00:
                return "(thin lock: " + toHex(mark) + ")";
            case 0b10:
                return "(fat lock: " + toHex(mark) + ")";
            case 0b01:
                String s = "; age: " + ((mark >> 3) & 0xF);
                int tribits = (int) (mark & 0b111);
                switch (tribits) {
                    case 0b001:
                        int hash = (int) (mark >>> 8);
                        if (hash != 0) {
                            return "(hash: " + toHex(hash) + s + ")";
                        } else {
                            return "(non-biasable" + s + ")";
                        }
                    case 0b101:
                        long thread = mark >>> 10;
                        if (thread == 0) {
                            return "(biasable" + s + ")";
                        } else {
                            return "(biased: " + toHex(thread) + "; epoch: " + ((mark >> 8) & 0x2) + s + ")";
                        }
                }
            default:
                return "(parse error)";
        }
    }

    // 无锁
    private static void noLock() {
        System.out.println("测试无锁");
        final Object obj = new Object();

        System.out.println("hashcode 16进制: " + Integer.toHexString(obj.hashCode()));
        System.out.println("hashcode 10进制: " + obj.hashCode());
        String str = Integer.toBinaryString(obj.hashCode());
        System.out.println("hashcode  2进制: " + ZERO_RUNS[32 - str.length()] + str);

        System.out.println(ClassLayout.parseInstance(obj).toPrintable());

        final long aLong = VM.current().getLong(obj, 0);
        System.out.println("10进制：" + aLong);
        System.out.println("16进制：" + toHexString(aLong));
        System.out.println(" 2进制：" + toBinaryString(aLong));
        // unused（25位）    hashcode（31位）    unused（1位）    分代年龄（4位）    偏向锁位（1位）    锁标志位（2位）
        System.out.println("unused（25位）    hashcode（31位）    unused（1位）    分代年龄（4位）    偏向锁位（1位）[0]    锁标志位（2位）[01]");
    }

    // biased lock
    // 偏向锁    java -XX:PrintFlagsInitial | grep BiasedLock*
    private static void biasLock() {
        try {
            TimeUnit.SECONDS.sleep(5L);
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
            throw new RuntimeException(e);
        }
        final CountDownLatch latch = new CountDownLatch(1);

        final Object obj = new Object();
//        new Thread(() -> {
        try {
//                synchronized (obj) {
            System.out.println("测试偏向锁，睡眠四秒后触发偏向锁");
            System.out.println(ClassLayout.parseInstance(obj).toPrintable());

            final long aLong = VM.current().getLong(obj, 0);
            System.out.println("10进制：" + aLong);
            System.out.println("16进制：" + toHexString(aLong));
            System.out.println(" 2进制：" + toBinaryString(aLong));
            System.out.println("当前线程指针JavaThread*（54位）    Epoch（2位）    unused（1位）    分代年龄（4位）    偏向锁位（1位）[1]    锁标志位（2位）[01]");
//                }
        } finally {
            latch.countDown();
        }
//        }, "偏向锁测试").start();

        try {
            latch.await();
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
            throw new RuntimeException(e);
        }
    }

    // thin lock
    private static void lightweightLock() {
        final Object obj = new Object();
        final CountDownLatch latch = new CountDownLatch(1);

        new Thread(() -> {
            try {
                synchronized (obj) {
                    System.out.println("测试轻量锁");
                    System.out.println(ClassLayout.parseInstance(obj).toPrintable());

                    final long aLong = VM.current().getLong(obj, 0);
                    System.out.println("10进制：" + aLong);
                    System.out.println("16进制：" + toHexString(aLong));
                    System.out.println(" 2进制：" + toBinaryString(aLong));
                    System.out.println("指向线程栈中Lock Record的指针（62位）    锁标志位（2位）[00]");
                }
            } finally {
                latch.countDown();
            }
        }, "轻量锁").start();

        try {
            latch.await();
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
            throw new RuntimeException(e);
        }
    }

    // fat lock
    private static void weightLock() {
        final Object obj = new Object();
        final int size = Runtime.getRuntime().availableProcessors();
        final CountDownLatch latch = new CountDownLatch(size);
        for (int i = 0; i < size; i++) {
            final int j = i;
            new Thread(() -> {
                try {
                    synchronized (obj) {
                        if (j == 2) {
                            System.out.println("测试重量锁");
                            System.out.println(ClassLayout.parseInstance(obj).toPrintable());

                            final long aLong = VM.current().getLong(obj, 0);
                            System.out.println("10进制：" + aLong);
                            System.out.println("16进制：" + toHexString(aLong));
                            System.out.println(" 2进制：" + toBinaryString(aLong));
                            System.out.println("指向互斥量（重量级锁）的指针（62位）    锁标志位（2位）[10]");
                        }
                    }
                } finally {
                    latch.countDown();
                }
            }, "重量锁" + j).start();
        }

        try {
            latch.await();
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
            throw new RuntimeException(e);
        }
    }

    // 当一个对象已经计算过identity hashcode，它就无法进入偏向锁状态，跳过偏向锁，直接升级轻量级锁
    @SuppressWarnings("SynchronizationOnLocalVariableOrMethodParameter")
    private static void example05() {
        try {
            TimeUnit.SECONDS.sleep(5L);
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
            throw new RuntimeException(e);
        }

        final Object obj = new Object();

        long aLong = VM.current().getLong(obj, 0);
        System.out.println("此时应该是偏向锁状态（101）。");
        System.out.println("10进制：" + aLong);
        System.out.println("16进制：" + toHexString(aLong));
        System.out.println(" 2进制：" + toBinaryString(aLong) + " " + parseMarkWord(aLong));

        System.out.println("没有重写hashcode，重写后无效，当一个对象已经计算过hashcode，他就无法进入偏向锁状态  " + obj.hashCode());

        synchronized (obj) {
            System.out.println("本来是偏向锁状态，由于计算过一致性哈希，所以会直接升级为轻量级锁（00）");
            aLong = VM.current().getLong(obj, 0);
            System.out.println("10进制：" + aLong);
            System.out.println("16进制：" + toHexString(aLong));
            System.out.println(" 2进制：" + toBinaryString(aLong) + " " + parseMarkWord(aLong));
        }
    }

    // 偏向锁过程中遇到一致性哈希计算请求，立马撤销偏向模式，膨胀为重量级锁
    @SuppressWarnings("SynchronizationOnLocalVariableOrMethodParameter")
    private static void example06() {
        try {
            TimeUnit.SECONDS.sleep(5L);
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
            throw new RuntimeException(e);
        }
        final Object obj = new Object();

        long aLong = VM.current().getLong(obj, 0);
        System.out.println("此时应该是偏向锁状态。");
        System.out.println("10进制：" + aLong);
        System.out.println("16进制：" + toHexString(aLong));
        System.out.println(" 2进制：" + toBinaryString(aLong) + " " + parseMarkWord(aLong));

        synchronized (obj) {
            System.out.println(obj.hashCode());

            System.out.println("偏向锁过程中遇到hashcode计算请求，立马撤销偏向模式，膨胀为重量级锁。");
            aLong = VM.current().getLong(obj, 0);
            System.out.println("10进制：" + aLong);
            System.out.println("16进制：" + toHexString(aLong));
            System.out.println(" 2进制：" + toBinaryString(aLong) + " " + parseMarkWord(aLong));
        }
    }

    // 锁消除
    @SuppressWarnings("SynchronizationOnLocalVariableOrMethodParameter")
    private static void example07() {
        final Object globalObj = new Object();

        for (int i = 0; i < 4 * 3; i++) {
            final Thread thread = new Thread(() -> {
                final Object localObj = new Object();

                // JIT编译器会无视它，（锁消除）
                // 这个锁对象没有被共享扩散到其它线程使用。

                synchronized (localObj) {
                    System.out.println("---- local " + localObj.hashCode() + "  ---- global " + globalObj.hashCode());
                }
            });
            thread.start();
        }
    }

    private static void example08() {
        // 锁粗化
        // 假如方法中首尾相接，前后相邻的都是同一个锁对象，那JIT编译器就会把这几个synchronized块合并成一个大块，加粗加大范围，一次申请锁使用即可，避免次次的申请和释放锁，提升了性能

        final Object obj = new Object();

        final Thread thread = new Thread(() -> {
            synchronized (obj) {
                System.out.println(111111);
            }
            synchronized (obj) {
                System.out.println(222222);
            }
            synchronized (obj) {
                System.out.println(333333);
            }
            synchronized (obj) {
                System.out.println(444444);
            }
            synchronized (obj) {
                System.out.println(555555);
            }
        });
        thread.start();
    }

    // 无锁 => 偏向锁 => 轻量锁 => 重量锁
    public static void main(String[] args) {
//        System.out.println("-----------------------------------------------------------------------------------------");
//        noLock();
//        System.out.println("-----------------------------------------------------------------------------------------");
//        biasLock();// java 15后开始废弃偏向锁
//        System.out.println("-----------------------------------------------------------------------------------------");
//        lightweightLock();// VM options 需添加 -XX:-UseBiasedLocking
//        System.out.println("-----------------------------------------------------------------------------------------");
//        weightLock();

//        example05();
//        System.out.println("-----------------------------------------------------------------------------------------");
//        example06();

//        example07();
        example08();
    }

}