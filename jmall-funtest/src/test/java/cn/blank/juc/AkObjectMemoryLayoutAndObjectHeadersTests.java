package cn.blank.juc;

import org.openjdk.jol.info.ClassLayout;
import org.openjdk.jol.vm.VM;

/**
 * Java对象内存布局和对象头
 */
public class AkObjectMemoryLayoutAndObjectHeadersTests {
    /*
     * 对象在堆内存中的存储布局
     *     对象头（在64位系统中，Mark Word占了8个字节，类型指针占了8个字节（未开启类型指针压缩），一共是16个字节。）
     *         对象标记    默认存储对象的HashCode、分代年龄和锁标志位等信息。
     *         类元信息    对象指向它的类元数据的指针，虚拟机通过这个指针来确定这个对象是哪个类的实例。
     *     实例数据
     *     实例填充    虚拟机要求对象起始地址必须是8字节的整数倍。
     *
     */


    public static void main(String[] args) {
        // 当前vm的信息
        System.out.println(VM.current().details());

        System.out.println("-----------------------------------------------------------------------------------------");
        // 所有的对象分配的字节都是8的整数倍
        System.out.println(VM.current().objectAlignment());
        System.out.println("-----------------------------------------------------------------------------------------");

        final Object obj = new Object();
        System.out.println("二进制hashcode：" + Integer.toBinaryString(obj.hashCode()));
        System.out.println("十进制hashcode：" + obj.hashCode());
        System.out.println("十六进制hashcode：" + Integer.toHexString(obj.hashCode()));
        // mark word 8字节
        // 类型指针4字节
//        System.out.println(ClassLayout.parseInstance(obj, new HotSpotLayouter(new Model64_CCPS(), 8)).toPrintable());
//        System.out.println("-----------------------------------------------------------------------------------------");
//        System.out.println(ClassLayout.parseInstance(obj, new RawLayouter(new Model64_CCPS())).toPrintable());
//        System.out.println("-----------------------------------------------------------------------------------------");
        System.out.println(ClassLayout.parseInstance(obj).toPrintable());
        System.out.println("-----------------------------------------------------------------------------------------");

        final Employee employee = new Employee();
        System.out.println(ClassLayout.parseInstance(employee).toPrintable());
    }

    @SuppressWarnings("unused")
    private static class Employee {
        private int id;
        private boolean flag;
    }

}