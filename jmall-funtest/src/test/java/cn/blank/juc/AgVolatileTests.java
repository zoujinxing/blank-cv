package cn.blank.juc;

import java.util.concurrent.TimeUnit;

/**
 * 指令关键字 volatile 作为指令关键字，确保本条指令不会因编译器的优化而省略，且要求每次直接读值。
 */
public class AgVolatileTests {

    /*
     * 1、保证可见性
     * 2、禁止指令重排（有序要求）
     *
     * 3、不保证原子性
     */

    private static volatile boolean FLAG = true;

    public static void main(String[] args) {
        final Thread thread = new Thread(() -> {
            while (FLAG) {
                try {
                    TimeUnit.MILLISECONDS.sleep(10L);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            System.out.println("当前子线程已退出");
        });
        thread.start();

        try {
            TimeUnit.SECONDS.sleep(3L);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        System.out.println("主线程开始终止子线程");
        FLAG = false;

        System.out.println("主线程已结束");
    }

}