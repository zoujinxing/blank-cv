package cn.blank.juc;

import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * java 锁
 */
@Slf4j
public class AcLockTests {
    public static final Object SYNC_LOCK = new Object();

    public static void main(String[] args) {
//        example01();
//        example02();
        example03();
//        example04();
    }

    private static void example01() {
        // 不公平锁
        final Lock nonFairLock = new ReentrantLock();
        try {
            nonFairLock.lock();

        } finally {
            nonFairLock.unlock();
        }

        // 公平锁
        final Lock fairLock = new ReentrantLock(true);
        try {
            fairLock.lock();

        } finally {
            fairLock.unlock();
        }
    }

    private static void example02() {
        try {
            syncMethod();
        } catch (InterruptedException e) {
            log.error(e.getMessage(), e);
        }
    }

    private static void syncMethod() throws InterruptedException {
        new Thread(() -> {
            synchronized (SYNC_LOCK) {
                System.out.println("您好，世界a！");
                try {
                    TimeUnit.SECONDS.sleep(2L);
                } catch (InterruptedException e) {
                    log.error(e.getMessage(), e);
                }
            }
        }, "A线程").start();

        TimeUnit.MILLISECONDS.sleep(10L);

        new Thread(() -> {
            synchronized (SYNC_LOCK) {
                System.out.println("您好，世界b！");
            }
        }, "B线程").start();
    }

    private static void example03() {
        /*
         * 题目：一个初始值0的变量，两个线程对其进行交替操作，一个加1，一个减1，循环5轮
         * 1、线程    操作（方法） 资源类
         * 2、判断    干活         通知
         * 3、防止线程虚假唤醒机制
         */

        final Lock reentrantLock = new ReentrantLock();
        final Condition condition = reentrantLock.newCondition();

        final int[] number = {0};

        final Runnable increment = () -> {
            reentrantLock.lock();
            try {
                // 判断
                while (number[0] != 0) {
                    try {
                        condition.await();
                    } catch (InterruptedException e) {
                        Thread.currentThread().interrupt();
                        e.printStackTrace();
                    }
                }

                // 干活
                number[0]++;
                System.out.println(Thread.currentThread().getName() + "    " + number[0]);

                // 通知唤醒
                condition.signalAll();
            } finally {
                reentrantLock.unlock();
            }
        };

        final Runnable decrement = () -> {
            reentrantLock.lock();
            try {
                // 判断
                while (number[0] == 0) {
                    try {
                        condition.await();
                    } catch (InterruptedException e) {
                        Thread.currentThread().interrupt();
                        e.printStackTrace();
                    }
                }

                // 干活
                number[0]--;
                System.out.println(Thread.currentThread().getName() + "    " + number[0]);

                // 通知唤醒
                condition.signalAll();
            } finally {
                reentrantLock.unlock();
            }
        };

        for (int i = 0; i < 5; i++) {
            new Thread(increment, "increment").start();
            new Thread(decrement, "decrement").start();
        }
    }

    /*
     * synchronized和lock有什么区别？用新的lock有什么好处？
     *
     * 1 原始构成
     *    synchronized是关键字属于jvm层面，monitorenter（底层是通过monitor对象来完成，其实wait/notify等方法也依赖于monitor对象，只有在同步块或方法中才能调用wait/notify等方法）
     *    lock是具体类(java.util.concurrent.locks.lock)是api层面的锁
     *
     * 2 使用方法
     *    synchronized 不需要用户去手动释放锁，当synchronized代码执行完后系统会自动让线程释放对锁的占用
     *    ReentrantLock 则需要用户去手动释放锁，若没有主动释放锁，就有可能导致出现死锁现象。需要lock()和unlock()方法配合try/finally语句块来完成。
     *
     * 3 等待是否可中断
     *    synchronized不可中断，除非抛出异常或者正常运行完成
     *    ReentrantLock 可中断，设置超时方法tryLock(long timeout,TimeUnit unit)。2）.lockInterruptibly()放代码块中，调用interrupt()方法可中断
     *
     * 4 加锁是否公平
     *    synchronized非公平锁
     *    ReentrantLock 两者都可以，默认非公平锁，构造方法可以传入boolean值，true为公平锁，false为非公平锁
     *
     * 5 锁绑定多个条件Condition
     *    synchronized没有
     *    ReentrantLock用来实现分组唤醒需要唤醒的线程们，可以精确唤醒，而不是像synchronized要么随机唤醒一个线程要么唤醒全部线程。
     */
    private static void example04() {
        /*
         * 题目：多线程之间按顺序调用，实现A->B->C
         * 三个线程启动，要求如下：
         * AA打印5次，BB打印10次，CC打印15次
         * 接着
         * AA打印5次，BB打印10次，CC打印15次
         * 三个线程来10轮
         */
        int[] number = {1};
        final Lock reentrantLock = new ReentrantLock();
        final Condition condition1 = reentrantLock.newCondition();
        final Condition condition2 = reentrantLock.newCondition();
        final Condition condition3 = reentrantLock.newCondition();

        final Runnable run1 = () -> {
            reentrantLock.lock();
            try {
                // 判断
                while (number[0] != 1) {
                    try {
                        condition1.await();
                    } catch (InterruptedException e) {
                        Thread.currentThread().interrupt();
                        e.printStackTrace();
                    }
                }

                // 干活
                for (int i = 1; i <= 5; i++) {
                    System.out.println(Thread.currentThread().getName() + "    " + i);
                }

                // 通知唤醒
                number[0] = 2;
                condition2.signal();
            } finally {
                reentrantLock.unlock();
            }
        };
        final Runnable run2 = () -> {
            reentrantLock.lock();
            try {
                // 判断
                while (number[0] != 2) {
                    try {
                        condition2.await();
                    } catch (InterruptedException e) {
                        Thread.currentThread().interrupt();
                        e.printStackTrace();
                    }
                }

                // 干活
                for (int i = 1; i <= 10; i++) {
                    System.out.println(Thread.currentThread().getName() + "    " + i);
                }

                // 通知唤醒
                number[0] = 3;
                condition3.signal();
            } finally {
                reentrantLock.unlock();
            }
        };
        final Runnable run3 = () -> {
            reentrantLock.lock();
            try {
                // 判断
                while (number[0] != 3) {
                    try {
                        condition3.await();
                    } catch (InterruptedException e) {
                        Thread.currentThread().interrupt();
                        e.printStackTrace();
                    }
                }

                // 干活
                for (int i = 1; i <= 15; i++) {
                    System.out.println(Thread.currentThread().getName() + "    " + i);
                }

                // 通知唤醒
                number[0] = 1;
                condition1.signal();
            } finally {
                reentrantLock.unlock();
            }
        };

        new Thread(() -> {
            for (int i = 0; i < 10; i++) {
                run1.run();
            }
        }, "thread-aa").start();
        new Thread(() -> {
            for (int i = 0; i < 10; i++) {
                run2.run();
            }
        }, "thread-bb").start();
        new Thread(() -> {
            for (int i = 0; i < 10; i++) {
                run3.run();
            }
        }, "thread-cc").start();
    }

}