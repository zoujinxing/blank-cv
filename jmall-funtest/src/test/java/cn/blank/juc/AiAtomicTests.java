package cn.blank.juc;

import java.util.concurrent.atomic.*;

/**
 * JDK 原子操作类
 */
public class AiAtomicTests {

    private static class Person {
        private String name;
        public volatile int id;
        public volatile long timestamp;
    }

    public static void main(String[] args) {
        final AtomicBoolean aBoolean = new AtomicBoolean();


        final AtomicInteger integer = new AtomicInteger();

        final AtomicIntegerArray integerArray = new AtomicIntegerArray(new int[10]);

        final AtomicIntegerFieldUpdater<Person> integerFieldUpdater = AtomicIntegerFieldUpdater.newUpdater(Person.class, "id");


        final AtomicLong aLong = new AtomicLong();

        final AtomicLongArray longArray = new AtomicLongArray(new long[10]);

        final AtomicLongFieldUpdater<Person> longFieldUpdater = AtomicLongFieldUpdater.newUpdater(Person.class, "timestamp");


        final AtomicReference<Person> atomicReference = new AtomicReference<>();

        final AtomicReferenceArray<Person> referenceArray = new AtomicReferenceArray<Person>(new Person[10]);

        final AtomicReferenceFieldUpdater<Person, Long> referenceFieldUpdater = AtomicReferenceFieldUpdater.newUpdater(Person.class, long.class, "timestamp");

        Person markablePerson = new Person();
        final AtomicMarkableReference<Person> markableReference = new AtomicMarkableReference<Person>(markablePerson, false);

        Person stampedPerson = new Person();
        final AtomicStampedReference<Person> stampedReference = new AtomicStampedReference<>(stampedPerson, 0);


        final DoubleAdder doubleAdder = new DoubleAdder();

        final DoubleAccumulator doubleAccumulator = new DoubleAccumulator(Double::sum, 0D);


        final LongAdder longAdder = new LongAdder();

        final LongAccumulator longAccumulator = new LongAccumulator((i, j) -> i - j, 0L);

    }

}