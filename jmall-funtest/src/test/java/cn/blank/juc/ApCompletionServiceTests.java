package cn.blank.juc;

import cn.hutool.core.util.RandomUtil;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.*;

/**
 * <br/>Date 2022/6/30
 * <br/>Time 8:44:50
 *
 * @author _blank
 */
@Slf4j
public class ApCompletionServiceTests {

    @Builder
    @ToString
    @EqualsAndHashCode
    private static class Person {
        private long cardId;
        private String name;
        private int age;
    }

    public static void main(String[] args) {
        final ExecutorService executor = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors());
        final BlockingQueue<Future<Person>> completionQueue = new LinkedBlockingQueue<>(128);

        final int taskNum = 4;
        final CompletionService<Person> completionService = new ExecutorCompletionService<>(executor, completionQueue);

        for (int i = 0; i < taskNum; i++) {
            completionService.submit(() -> {
                final long timeout = RandomUtil.randomLong(99L, 999L);
                TimeUnit.MILLISECONDS.sleep(timeout);
                return Person.builder().cardId(timeout).name(RandomUtil.randomString(8)).age(RandomUtil.randomInt(0, 100)).build();
            });
        }

        try {
            for (int i = 0; i < taskNum; i++) {
                final Person person = completionService.take().get();
                log.info("{}", person);
            }
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
            log.error(e.getMessage(), e);
        } catch (ExecutionException e) {
            log.error(e.getMessage(), e);
        } finally {
            executor.shutdown();
        }
    }

}
