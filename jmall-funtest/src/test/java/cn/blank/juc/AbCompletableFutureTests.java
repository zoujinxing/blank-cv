package cn.blank.juc;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * 基于事件驱动的异步回调类
 */
public class AbCompletableFutureTests {

    public static void main(String[] args) {
        final ExecutorService executor = Executors.newFixedThreadPool(1);

        final CompletableFuture<String> async01 = CompletableFuture.supplyAsync(() -> "");
        final CompletableFuture<String> async06 = CompletableFuture.supplyAsync(() -> "", executor);

        final CompletableFuture<String> async02 = CompletableFuture.completedFuture("");

        final CompletableFuture<Void> async05 = CompletableFuture.runAsync(System.out::println);

        final CompletableFuture<Void> async03 = CompletableFuture.allOf();

        final CompletableFuture<Object> async04 = CompletableFuture.anyOf();
    }

}