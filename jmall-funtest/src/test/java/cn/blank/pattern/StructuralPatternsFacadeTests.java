package cn.blank.pattern;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;

/**
 * @author _blank    DateTime 2022/7/5 17:45:57
 */
@Slf4j
class StructuralPatternsFacadeTests {
    /* 结构型模式 -- 外观模式 */

    private static class PoliceDepartment {
        void register(String name) {
            log.info("{}您好，已经帮您办理申请落户！", name);
        }
    }

    private static class SocialSecurityBureau {
        void handlerSocial(String name) {
            log.info("{}您好，已经帮您的社保办理转移！", name);
        }
    }

    private static class EducationBureau {
        void assignSchool(String name) {
            log.info("{}您好，已经帮您的孩子分配好学校！", name);
        }
    }

    @SuppressWarnings("SameParameterValue")
    private static class WechatFacade {
        private final PoliceDepartment policeDepartment;
        private final SocialSecurityBureau socialSecurityBureau;
        private final EducationBureau educationBureau;

        WechatFacade(final PoliceDepartment policeDepartment, final SocialSecurityBureau socialSecurityBureau, final EducationBureau educationBureau) {
            this.policeDepartment = policeDepartment;
            this.socialSecurityBureau = socialSecurityBureau;
            this.educationBureau = educationBureau;
        }

        void handleXxx(String name) {
            this.policeDepartment.register(name);
            this.socialSecurityBureau.handlerSocial(name);
            this.educationBureau.assignSchool(name);
        }

        void policeDepartmentRegister(String name) {
            policeDepartment.register(name);
        }

        void socialSecurityBureauHandlerSocial(String name) {
            socialSecurityBureau.handlerSocial(name);
        }

        void educationBureauAssignSchool(String name) {
            educationBureau.assignSchool(name);
        }
    }

    @Test
    void testFacade() {
        final WechatFacade wechatFacade = new WechatFacade(new PoliceDepartment(), new SocialSecurityBureau(), new EducationBureau());
        wechatFacade.handleXxx("张三丰");
        wechatFacade.handleXxx("李思思");

        wechatFacade.policeDepartmentRegister("王五万");
        wechatFacade.socialSecurityBureauHandlerSocial("赵柳月");
        wechatFacade.educationBureauAssignSchool("孙琪琪");
    }
}
