package cn.blank.pattern;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

/**
 * @author _blank    DateTime 2022/7/5 17:37:09
 */
@Slf4j
class BehavioralPatternsVisitorTests {
    /* 行为型模式 -- 访问者模式 */

    /* 抽象元素 */
    private static abstract class AbstractRobotHardware {
        /* 接收指令 */
        String command;

        AbstractRobotHardware(String command) {
            this.command = command;
        }

        void acceptCommand(String command) {
            this.command += command;
        }

        /* 接收指令后做出相应的响应 */
        void work() {
            log.info("{}接收到的指令：{}", this.getClass().getSimpleName(), this.command);
        }

        /* 访问模式    具体硬件做具体的响应 */
        abstract void accept(Visitor visitor);
    }

    private static class CPUHardware extends AbstractRobotHardware {
        CPUHardware(final String command) {
            super(command);
        }

        @Override
        void accept(final Visitor visitor) {
            visitor.visitorCPU(this);
        }
    }

    private static class DiskHardware extends AbstractRobotHardware {
        DiskHardware(final String command) {
            super(command);
        }

        @Override
        void accept(final Visitor visitor) {
            visitor.visitorDisk(this);
        }
    }

    private static class MemoryHardware extends AbstractRobotHardware {
        MemoryHardware(final String command) {
            super(command);
        }

        @Override
        void accept(final Visitor visitor) {
            visitor.visitorMemory(this);
        }
    }

    /* 抽象访问者 */
    private interface Visitor {
        /* 访问者访问元素 */

        void visitorCPU(CPUHardware cpuHardware);

        void visitorDisk(DiskHardware diskHardware);

        void visitorMemory(MemoryHardware memoryHardware);
    }

    private static class UpdatePackage implements Visitor {
        private final String updateInfo;

        UpdatePackage(final String updateInfo) {
            this.updateInfo = updateInfo;
        }

        @Override
        public void visitorCPU(final CPUHardware cpuHardware) {
            cpuHardware.command += " >>> " + this.updateInfo;
        }

        @Override
        public void visitorDisk(final DiskHardware diskHardware) {
            diskHardware.command += " >>> >>> " + this.updateInfo;
        }

        @Override
        public void visitorMemory(final MemoryHardware memoryHardware) {
            memoryHardware.command += " >>> >>> >>> " + this.updateInfo;
        }
    }


    /* 对象结构 */
    private static class XiaoAi {
        private final AbstractRobotHardware cpuHardware;
        private final AbstractRobotHardware diskHardware;
        private final AbstractRobotHardware memoryHardware;

        public XiaoAi() {
            final String command = "广州天气";

            this.cpuHardware = new CPUHardware(command);
            this.diskHardware = new DiskHardware(command);
            this.memoryHardware = new MemoryHardware(command);
        }

        void answerQuestion(String command) {
            this.cpuHardware.acceptCommand(command);
            this.diskHardware.acceptCommand(command);
            this.memoryHardware.acceptCommand(command);

            this.cpuHardware.work();
            this.diskHardware.work();
            this.memoryHardware.work();
        }

        void acceptUpdate(Visitor aPackage) {
            /* 访问模式    升级小爱 */
            this.cpuHardware.accept(aPackage);
            this.diskHardware.accept(aPackage);
            this.memoryHardware.accept(aPackage);
        }
    }

    @Test
    void testVisitor() {
        final XiaoAi xiaoAi = new XiaoAi();
        xiaoAi.answerQuestion(" 天河区天气");

        xiaoAi.acceptUpdate(new UpdatePackage("联网增强功能"));

        xiaoAi.answerQuestion(" 番禺区天气");
    }

    // -----------------------------------------------------------------------------------------------------------------
    /* 加强理解 */

    /* 员工类：抽象元素类 */
    private interface Employee {
        /* 接受一个抽象访问者访问 */
        void accept(Department department);
    }

    private static class EmployeeInfo {
        /* 员工名称 */
        private final String name;
        /* 工作时间 */
        private final int workTime;

        EmployeeInfo(final String name, final int workTime) {
            this.name = name;
            this.workTime = workTime;
        }

        String getName() {
            return name;
        }

        int getWorkTime() {
            return workTime;
        }
    }

    /* 全职员工类：具体元素类 */
    private static class FullTimeEmployee extends EmployeeInfo implements Employee {
        /* 每周工资 */
        private final double weeklyWage;

        FullTimeEmployee(final String name, final double weeklyWage, final int workTime) {
            super(name, workTime);
            this.weeklyWage = weeklyWage;
        }

        public double getWeeklyWage() {
            return weeklyWage;
        }

        @Override
        public void accept(final Department department) {
            department.visit(this);
        }
    }

    /* 兼职员工类：具体元素类 */
    private static class PartTimeEmployee extends EmployeeInfo implements Employee {
        /* 小时工资 */
        private final double hourWage;

        PartTimeEmployee(final String name, final double hourWage, final int workTime) {
            /* 调用访问者的访问方法 */
            super(name, workTime);
            this.hourWage = hourWage;
        }

        public double getHourWage() {
            return hourWage;
        }

        @Override
        public void accept(final Department department) {
            /* 调用访问者的访问方法 */
            department.visit(this);
        }
    }

    /* 部门类：抽象访问者类 */
    private static abstract class Department {
        /* 声明一组重载的访问方法，用于访问不同类型的具体元素 */

        abstract void visit(final FullTimeEmployee fullTimeEmployee);

        abstract void visit(final PartTimeEmployee partTimeEmployee);
    }

    /* 财务部类：具体访问者类 */
    private static class FaDepartment extends Department {
        @Override
        void visit(final FullTimeEmployee fullTimeEmployee) {
            /* 实现财务部对全职员工的访问 */
            final int workTime = fullTimeEmployee.getWorkTime();
            double weeklyWage = fullTimeEmployee.getWeeklyWage();
            if (workTime > 40) {
                weeklyWage = weeklyWage + (workTime - 40) * 100;
            } else if (workTime < 40) {
                weeklyWage = weeklyWage - (40 - workTime) * 80;
                if (weeklyWage < 0) {
                    weeklyWage = 0;
                }
            }
            log.info("正式员工 {} 实际工资为：{} 元。", fullTimeEmployee.getName(), weeklyWage);
        }

        @Override
        void visit(final PartTimeEmployee partTimeEmployee) {
            /* 实现财务部对兼职员工的访问 */
            final int workTime = partTimeEmployee.getWorkTime();
            final double hourWage = partTimeEmployee.getHourWage();
            log.info("临时工 {} 实际工资为：{} 元。", partTimeEmployee.getName(), workTime * hourWage);
        }
    }

    /* 人力资源部类：具体访问者类 */
    private static class HrDepartment extends Department {
        @Override
        void visit(final FullTimeEmployee fullTimeEmployee) {
            /* 实现人力资源部对全职员工的访问 */
            final int workTime = fullTimeEmployee.getWorkTime();
            log.info("正式员工 {} 实际工作时间为：{} 小时。", fullTimeEmployee.getName(), workTime);
            if (workTime > 40) {
                log.info("正式员工 {} 加班时间为：{} 小时。", fullTimeEmployee.getName(), (workTime - 40));
            } else if (workTime < 40) {
                log.info("正式员工 {} 请假时间为：{} 小时。", fullTimeEmployee.getName(), (40 - workTime));
            }
        }

        @Override
        void visit(final PartTimeEmployee partTimeEmployee) {
            /* 实现人力资源部对兼职员工的访问 */
            final int workTime = partTimeEmployee.getWorkTime();
            log.info("临时工 {} 实际工作时间为：{} 小时。", partTimeEmployee.getName(), workTime);
        }
    }

    /* 员工列表类：对象结构 */
    private static class EmployeeList {
        /* 定义一个集合用于存储员工对象 */
        private final List<Employee> list;

        EmployeeList(Employee... employees) {
            this.list = Arrays.asList(employees);
        }

        /* 遍历访问员工集合中的每一个员工对象 */
        void accept(Department department) {
            list.forEach(employee -> employee.accept(department));
        }
    }

    @Test
    void testVisit() {
        final FullTimeEmployee fte1 = new FullTimeEmployee("张三", 3200D, ThreadLocalRandom.current().nextInt(35, 45));
        final FullTimeEmployee fte2 = new FullTimeEmployee("李四", 2400D, ThreadLocalRandom.current().nextInt(35, 45));
        final FullTimeEmployee fte3 = new FullTimeEmployee("王五", 2800D, ThreadLocalRandom.current().nextInt(35, 45));
        final PartTimeEmployee pte1 = new PartTimeEmployee("赵六", 70D, ThreadLocalRandom.current().nextInt(15, 25));
        final PartTimeEmployee pte2 = new PartTimeEmployee("孙七", 80D, ThreadLocalRandom.current().nextInt(15, 25));
        final PartTimeEmployee pte3 = new PartTimeEmployee("周八", 90D, ThreadLocalRandom.current().nextInt(15, 25));

        log.info("-------------------------------------------------------------------------------------\n以下财务部计算");
        final EmployeeList employeeList = new EmployeeList(fte1, fte2, fte3, pte1, pte2, pte3);
        employeeList.accept(new FaDepartment());

        log.info("-----------------------------------------------------------------------------------\n以下人力资源计算");
        employeeList.accept(new HrDepartment());
    }
}
