package cn.blank.pattern;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.ThreadLocalRandom;

/**
 * @author _blank    DateTime 2022/7/5 17:46:13
 */
@Slf4j
class StructuralPatternsFlyweightTests {
    /* 结构型模式 -- 享元模式 */

    private static abstract class AbstractWaitressFlyweight {
        private boolean canService = true;

        boolean isCanService() {
            return canService;
        }

        void setCanService(final boolean canService) {
            this.canService = canService;
        }

        /* 正在服务 */
        abstract void service();

        /* 服务完毕 */
        abstract void end();
    }

    private static class BeautifulWaitress extends AbstractWaitressFlyweight {
        String id;
        String name;
        int age;

        BeautifulWaitress(final String id, final String name, final int age) {
            this.id = id;
            this.name = name;
            this.age = age;
        }

        @Override
        void service() {
            log.info("工号：{}；年龄为{}岁的{}姑娘为您服务！", this.id, this.age, this.name);
            super.setCanService(false);
        }

        @Override
        void end() {
            log.info("工号：{}；年龄为{}岁的{}姑娘服务完毕，请给五星好评！", this.id, this.age, this.name);
            super.setCanService(true);
        }
    }

    private static class Footpath {
        private static final Map<String, AbstractWaitressFlyweight> POOL = new HashMap<>();

        static {
            final BeautifulWaitress xiaoHua = new BeautifulWaitress("1234", "小花", 18);
            final BeautifulWaitress xiaoCui = new BeautifulWaitress("4051", "小翠", 20);
            final BeautifulWaitress xiaoFang = new BeautifulWaitress("6154", "小芳", 21);
            POOL.put(xiaoHua.id, xiaoHua);
            POOL.put(xiaoCui.id, xiaoCui);
            POOL.put(xiaoFang.id, xiaoFang);
        }

        static AbstractWaitressFlyweight getWaitress(String id) {
            final AbstractWaitressFlyweight flyweight = POOL.get(id);
            if (Objects.isNull(flyweight)) {
                for (final Map.Entry<String, AbstractWaitressFlyweight> entry : POOL.entrySet()) {
                    if (entry.getValue().isCanService()) {
                        return entry.getValue();
                    }
                }
                log.info("全部技师繁忙中，请稍等……");
                return null;
            }
            if (flyweight.isCanService()) {
                return flyweight;
            }
            log.info("{}技师繁忙中，请稍等……", id);
            return null;
        }

        static void addWaitress(AbstractWaitressFlyweight waitressFlyweight) {
            final String name = new String(new char[]{randomChinese(), randomChinese(), randomChinese()});
            final BeautifulWaitress waitress = new BeautifulWaitress(randomInt(8), name, randomInt(18, 32));
            POOL.put(waitress.id, waitress);
        }
    }

    private static String randomInt(int limit) {
        return String.valueOf(ThreadLocalRandom.current().nextInt(limit));
    }

    private static int randomInt(int min, int max) {
        return ThreadLocalRandom.current().nextInt(min, max);
    }

    private static char randomChinese() {
        return (char) ThreadLocalRandom.current().nextInt('\u4E00', '\u9FFF');
    }

    @Test
    void testFlyweight() {
        try {
            final AbstractWaitressFlyweight waitress1 = Footpath.getWaitress("");
            waitress1.service();

            final AbstractWaitressFlyweight waitress2 = Footpath.getWaitress("");
            try {
                waitress2.service();

                final AbstractWaitressFlyweight waitress3 = Footpath.getWaitress("6154");
                waitress3.service();
                waitress3.end();

            } finally {
                waitress2.end();
            }

            waitress1.end();
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }
}
