package cn.blank.pattern;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;

import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * @author _blank    DateTime 2022/7/5 15:29:08
 */
@Slf4j
class BehavioralPatternsMediatorTests {
    /* 行为型模式 -- 中介者模式 */

    /**
     * 抽象机长
     */
    private static abstract class Captain {
        //起飞
        abstract void fly();

        //降落
        abstract void land();
    }

    /**
     * 塔台：中介者
     * 网状变为星状
     */
    private static class ControlTower {
        private final AtomicBoolean canDo = new AtomicBoolean(true);
        private final Semaphore semaphore = new Semaphore(1);// 塔台一次允许起飞或者降落的飞机数

        //接受请求...
        void acceptRequest(Captain captain, String action) {
            if (!this.canDo.get()) {
                log.info("{}，您好，机场正在被使用中，请稍等~~~", captain.getClass().getSimpleName());
            }
            try {
                this.semaphore.acquire();

                // 塔台标识机场正在被使用
                this.canDo.compareAndSet(true, false);

                log.info("塔台允许 {} {}！", captain.getClass().getSimpleName(), action);
                try {
                    TimeUnit.SECONDS.sleep(2L);
                } catch (InterruptedException e) {
                    Thread.currentThread().interrupt();
                    log.error(e.getMessage(), e);
                }
                log.info("飞机 {} {} 完成！", captain.getClass().getSimpleName(), action);
            } catch (InterruptedException e) {
                Thread.currentThread().interrupt();
                log.error(e.getMessage(), e);
            } finally {
                this.canDo.compareAndSet(false, true);
                this.semaphore.release();
            }
        }
    }

    /**
     * 海南8778
     */
    private static class HU8778 extends Captain {
        ControlTower controlTower;

        HU8778(ControlTower controlTower) {
            this.controlTower = controlTower;
        }

        @Override
        void fly() {
            log.info("HU8778 请求起飞......");
            //问每个机长能否起飞？
            controlTower.acceptRequest(this, "fly");
        }

        @Override
        void land() {
            log.info("HU8778 请求降落......");
            controlTower.acceptRequest(this, "land");
        }
    }

    /**
     * 四川8633机长
     */
    private static class SC8633 extends Captain {
        ControlTower controlTower;

        SC8633(ControlTower controlTower) {
            this.controlTower = controlTower;
        }

        @Override
        void fly() {
            log.info("SC8633 请求起飞......");
            //问每个机长能否起飞？
            controlTower.acceptRequest(this, "fly");
        }

        @Override
        void land() {
            log.info("SC8633 请求降落......");
            //问每个机长能否起飞？
            controlTower.acceptRequest(this, "land");
        }
    }

    /**
     * 星星9527
     */
    private static class XC9527 extends Captain {
        ControlTower controlTower;

        XC9527(ControlTower controlTower) {
            this.controlTower = controlTower;
        }

        @Override
        void fly() {
            log.info("XC9527 请求起飞....");
            //问每个机长能否起飞？
            controlTower.acceptRequest(this, "fly");
        }

        @Override
        void land() {
            log.info("XC9527 请求降落....");
            //问每个机长能否起飞？
            controlTower.acceptRequest(this, "land");
        }
    }

    @Test
    void testMediator() {
        // 中介者模式
        final ControlTower tower = new ControlTower();

        final HU8778 hu8778 = new HU8778(tower);
        final SC8633 sc8633 = new SC8633(tower);
        final XC9527 xc9527 = new XC9527(tower);

        final ExecutorService executorService = Executors.newFixedThreadPool(3);
        CompletableFuture.allOf(
                CompletableFuture.runAsync(hu8778::fly, executorService)
                , CompletableFuture.runAsync(sc8633::fly, executorService)
                , CompletableFuture.runAsync(xc9527::fly, executorService)

                , CompletableFuture.runAsync(hu8778::land, executorService)
                , CompletableFuture.runAsync(sc8633::land, executorService)
                , CompletableFuture.runAsync(xc9527::land, executorService)
        ).join();
        executorService.shutdown();
    }

}
