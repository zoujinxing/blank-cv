package cn.blank.pattern;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.ThreadLocalRandom;

/**
 * @author _blank    DateTime 2022/7/5 17:37:31
 */
@Slf4j
class BehavioralPatternsMementoTests {
    /* 行为型模式 -- 备忘录模式 */

    /* 备忘录 */
    private static class GameRecord {
        /* 记录id */
        private final Integer id;
        /* 剩余金币 */
        private final Integer coin;
        /* 血量 */
        private final Integer hp;
        /* 蓝量 */
        private final Integer mp;
        /* 等级 */
        private final Integer level;

        GameRecord(final Integer id, final Integer coin, final Integer hp, final Integer mp, final Integer level) {
            this.id = id;
            this.coin = coin;
            this.hp = hp;
            this.mp = mp;
            this.level = level;
        }

        Integer getId() {
            return id;
        }

        Integer getCoin() {
            return coin;
        }

        Integer getHp() {
            return hp;
        }

        Integer getMp() {
            return mp;
        }

        Integer getLevel() {
            return level;
        }

        @Override
        public String toString() {
            return "GameRecord{" + "id=" + id + ", coin=" + coin + ", hp=" + hp + ", mp=" + mp + ", level=" + level + '}';
        }

        String getCurrent() {
            return this.toString();
        }
    }

    private static class GameServer {
        private final Map<Integer, GameRecord> records = new HashMap<>();
        private int i;

        Gamer getRecord(Integer id) {
            final GameRecord gameRecord = this.records.get(id);
            if (Objects.isNull(gameRecord)) {
                return null;
            }
            log.info("从备忘录读取到的存档信息为：{}", gameRecord.getCurrent());
            // 属性拷贝
            return new ZhangSanGamer(gameRecord.getCoin(), gameRecord.getHp(), gameRecord.getMp(), gameRecord.getLevel());
        }

        void addRecord(Gamer gamer) {
            // 属性拷贝
            final GameRecord gameRecord = new GameRecord(this.i++, gamer.getCoin(), gamer.getHp(), gamer.getMp(), gamer.getLevel());
            this.records.putIfAbsent(gameRecord.getId(), gameRecord);
        }
    }

    private static abstract class Gamer {
        /* 剩余金币 */
        protected Integer coin;
        /* 血量 */
        protected Integer hp;
        /* 蓝量 */
        protected Integer mp;
        /* 等级 */
        protected Integer level;

        Integer getCoin() {
            return coin;
        }

        Integer getHp() {
            return hp;
        }

        Integer getMp() {
            return mp;
        }

        Integer getLevel() {
            return level;
        }
    }

    private static class ZhangSanGamer extends Gamer {
        private final GameServer gameServer = new GameServer();

        ZhangSanGamer(final Integer coin, final Integer hp, final Integer mp, final Integer level) {
            super.coin = coin;
            super.hp = hp;
            super.mp = mp;
            super.level = level;
        }

        ZhangSanGamer() {
        }

        void saveGameRecord() {
            log.info("正在保存游戏记录！");
            this.gameServer.addRecord(this);
        }

        Gamer getFromMemento(Integer id) {
            log.info("正在获取历史存档信息！");
            return this.gameServer.getRecord(id);
        }

        void playGame() {
            final int i = ThreadLocalRandom.current().nextInt(10000, 99999);
            log.info("游戏中。。。。。。。。{}", i);
            super.coin = i;
            super.hp = i;
            super.mp = i;
            super.level = i;
        }

        void exitGame() {
            log.info("正在保存并退出游戏");
            this.saveGameRecord();
        }
    }

    @Test
    void testMemento() {
        final ZhangSanGamer zhangSanGamer = new ZhangSanGamer();

        zhangSanGamer.playGame();

        zhangSanGamer.saveGameRecord();

        zhangSanGamer.playGame();

        zhangSanGamer.playGame();

        zhangSanGamer.saveGameRecord();

        zhangSanGamer.playGame();

        zhangSanGamer.saveGameRecord();

        log.info("读{}档继续玩~~~", 1);
        final Gamer fromMemento1 = zhangSanGamer.getFromMemento(1);
        ((ZhangSanGamer) fromMemento1).playGame();
        ((ZhangSanGamer) fromMemento1).saveGameRecord();

        log.info("读{}档继续玩~~~", 2);
        final Gamer fromMemento2 = zhangSanGamer.getFromMemento(2);
        ((ZhangSanGamer) fromMemento2).playGame();
        ((ZhangSanGamer) fromMemento2).saveGameRecord();

        log.info("-");
    }
}
