package cn.blank.pattern;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;

/**
 * @author _blank    DateTime 2022/7/5 10:35:40
 */
@Slf4j
class BehavioralPatternsObserverTests {
    /* 行为型模式 -- 观察者模式 */

    /**
     * 粉丝抽象类
     */
    private static abstract class AbstractFans /*implements Observer*/ {
        AbstractFans subscribe(AbstractAnchor abstractAnchor) {
            abstractAnchor.accept(this);
            return this;
        }

        void anchorSays(String msg) {
            log.info("{}粉丝接到主播的通知：{}", this.getClass().getSimpleName(), msg);
        }

        abstract void fansSay(AbstractAnchor abstractAnchor);
    }

    private static class FansOne extends AbstractFans {
        @Override
        void fansSay(AbstractAnchor abstractAnchor) {
            //noinspection ResultOfMethodCallIgnored
            abstractAnchor.fansMsg.offer("我是榜一");
        }
    }

    private static class FansTwo extends AbstractFans {
        @Override
        void fansSay(AbstractAnchor abstractAnchor) {
            //noinspection ResultOfMethodCallIgnored
            abstractAnchor.fansMsg.offer("我是榜二");
        }
    }

    private static class FansThree extends AbstractFans {
        @Override
        void fansSay(AbstractAnchor abstractAnchor) {
            //noinspection ResultOfMethodCallIgnored
            abstractAnchor.fansMsg.offer("我是榜三");
        }
    }

    /**
     * 主播抽象类
     */
    private static abstract class AbstractAnchor /*extends Observable*/ {
        private final List<AbstractFans> fansList;
        private final BlockingQueue<String> fansMsg;

        AbstractAnchor() {
            this.fansList = new ArrayList<>();
            this.fansMsg = new LinkedBlockingQueue<>();
        }

        void accept(final AbstractFans abstractFans) {
            if (abstractFans == null) {
                throw new IllegalArgumentException("粉丝不能为空！");
            }
            if (!this.fansList.contains(abstractFans)) {
                this.fansList.add(abstractFans);
            }
        }

        void notifyFans(String msg) {
            for (AbstractFans fans : this.fansList) {
                fans.anchorSays(msg);
            }
        }

        void acceptFansMsg() {
            try {
                for (int i = 0; i < 15; i++) {
                    String msg = this.fansMsg.take();
                    log.info("粉丝说：{}", msg);
                }
            } catch (InterruptedException e) {
                Thread.currentThread().interrupt();
                throw new RuntimeException(e);
            }
        }
    }

    private static class LdbAnchor extends AbstractAnchor {
        void startLiveStreaming() {
            log.info("陈翔六点半开始直播了~~~");
            super.notifyFans("我正在直播~~~");
        }

        void stopLiveStreaming() {
            log.info("陈翔六点半结束直播~~~");
            super.notifyFans("我的直播结束了~~~");
        }
    }

    @Test
    void testObserver() {
        LdbAnchor ldbAnchor = new LdbAnchor();

        new Thread(ldbAnchor::acceptFansMsg, "主播").start();

        ldbAnchor.startLiveStreaming();
        new FansOne().subscribe(ldbAnchor).fansSay(ldbAnchor);
        new FansTwo().subscribe(ldbAnchor).fansSay(ldbAnchor);
        new FansThree().subscribe(ldbAnchor).fansSay(ldbAnchor);
        try {
            TimeUnit.SECONDS.sleep(1L);
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
            log.error(e.getMessage(), e);
        }
        ldbAnchor.stopLiveStreaming();
    }

}
