package cn.blank.pattern;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * @author _blank    DateTime 2022/7/5 16:47:13
 */
@Slf4j
class BehavioralPatternsIteratorTests {
    /* 行为型模式 -- 迭代器模式 */

    private interface IteratorExt<E> extends Iterator<E> {
        /* 当前元素 */
        E lastElement();

        /* 第一个元素 */
        E firstElement();
    }

    private static class MyIterator<E> implements IteratorExt<E> {
        private int cursor = 0;
        private final List<E> element;

        MyIterator(final List<E> element) {
            this.element = element;
        }

        @Override
        public E lastElement() {
            return this.element.get(this.element.size() - 1);
        }

        @Override
        public E firstElement() {
            return this.element.get(0);
        }

        @Override
        public boolean hasNext() {
            return cursor < this.element.size();
        }

        @Override
        public E next() {
            return this.element.get(cursor++);
        }
    }

    private static class MakeFriends {
        private final List<String> friends = new ArrayList<>();

        IteratorExt<String> iterator() {
            return new MyIterator<>(this.friends);
        }

        void knowledge(String name) {
            this.friends.add(name);
        }

        void tearUp(String name) {
            this.friends.remove(name);
        }
    }

    @Test
    void testIterator() {
        final MakeFriends makeFriends = new MakeFriends();
        makeFriends.knowledge("张三");
        makeFriends.knowledge("李四");
        makeFriends.knowledge("王五");
        makeFriends.knowledge("赵六");
        makeFriends.knowledge("孙七");
        makeFriends.knowledge("周八");
        makeFriends.knowledge("吴九");
        makeFriends.knowledge("郑十");

        makeFriends.tearUp("赵六");
        makeFriends.tearUp("周八");

        IteratorExt<String> iter;
        for (iter = makeFriends.iterator(); iter.hasNext(); ) {
            final String next = iter.next();
            log.info("结识新朋友：{}", next);
        }

        log.info("第一个朋友：{}", iter.firstElement());
        log.info("最后一个朋友：{}", iter.lastElement());
    }

}
