package cn.blank.pattern;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;

/**
 * <br/>Date 2022/7/1
 * <br/>Time 18:14:44
 *
 * @author _blank
 */
@Slf4j
class BehavioralPatternsStateTests {
    /* 行为型模式 -- 状态模式 */

    /**
     * 抽象状态
     */
    private interface TeamState {
        //玩游戏
        void playGame();

        //切换到下一个状态
        TeamState next();
    }

    /**
     * 吃牛肉面状态
     */
    private static class BeefNoodlesState implements TeamState {
        @Override
        public void playGame() {
            log.info("饱饱的一顿牛肉面......中了诅咒，输了");
        }

        @Override
        public TeamState next() {
            return new MatchState();
        }
    }

    /**
     * 竞赛状态
     */
    private static class MatchState implements TeamState {
        @Override
        public void playGame() {
            log.info("全力以赴打比赛....");
        }

        //状态模式的核心
        @Override
        public TeamState next() {
            return new VocationState();
        }
    }

    /**
     * 休假状态
     */
    private static class VocationState implements TeamState {
        @Override
        public void playGame() {
            log.info("三亚旅游真舒服....饿了...不玩游戏");
            //状态流转
        }

        @Override
        public TeamState next() {
            return new BeefNoodlesState();
        }
    }

    /**
     * 环境类：
     */
    private static class SKTTeam {
        private TeamState teamState;

        void setTeamState(TeamState teamState) {
            this.teamState = teamState;
        }

        //开始游戏
        void startGame() {
            //状态不同会导致不同的游戏结果
            teamState.playGame();
        }

        //下一个状态
        void nextState() {
            teamState = teamState.next();
        }
    }

    @Test
    void testState() {
        final SKTTeam sktTeam = new SKTTeam();
        TeamState state;

        state = new VocationState();
        sktTeam.setTeamState(state);
        sktTeam.startGame();

        state = state.next();
        sktTeam.setTeamState(state);
        sktTeam.startGame();

        state = state.next();
        sktTeam.setTeamState(state);
        sktTeam.startGame();
    }

}
