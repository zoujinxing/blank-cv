package cn.blank.pattern;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

@Slf4j
class CreationalPatternsFactoryMethodTests {
    /**
     * 创造型模式  --  工厂方法模式
     */

    private static abstract class AbstractCar {
        private final String engine;

        public AbstractCar(final String engine) {
            this.engine = engine;
        }

        public String getEngine() {
            return engine;
        }

        abstract void run();
    }

    private static class MiniCar extends AbstractCar {
        MiniCar() {
            super("微型车 - 水平对置四缸发动机");
        }

        @Override
        void run() {
            log.info("{}    {}", super.getEngine(), "嘟嘟嘟~~~");
        }
    }

    private static class RacingCar extends AbstractCar {
        RacingCar() {
            super("赛车 - V8发动机");
        }

        @Override
        void run() {
            log.info("{}    {}", super.getEngine(), "嗖嗖嗖~~~");
        }
    }

    private static class VanCar extends AbstractCar {
        VanCar() {
            super("面包车 - 单杠柴油机");
        }

        @Override
        void run() {
            log.info("{}    {}", super.getEngine(), "嗒嗒嗒~~~");
        }
    }

    private static abstract class AbstractCarFactory {
        /* 生产汽车 */
        abstract AbstractCar newCar();
    }

    private static class MiniCarFactory extends AbstractCarFactory {
        @Override
        AbstractCar newCar() {
            return new MiniCar();
        }
    }

    private static class RacingCarFactory extends AbstractCarFactory {
        @Override
        AbstractCar newCar() {
            return new RacingCar();
        }
    }

    private static class VanCarFactory extends AbstractCarFactory {
        @Override
        AbstractCar newCar() {
            return new VanCar();
        }
    }

    @Test
    void testFactoryMethod() {
        final Class<?>[] allClass = {
                CreationalPatternsFactoryMethodTests.MiniCarFactory.class
                , CreationalPatternsFactoryMethodTests.RacingCarFactory.class
                , CreationalPatternsFactoryMethodTests.VanCarFactory.class
        };
        for (final Class<?> aClass : allClass) {
            try {
                final Constructor<?> declaredConstructor = aClass.getDeclaredConstructor();
                declaredConstructor.setAccessible(true);
                final AbstractCarFactory abstractCarFactory = (AbstractCarFactory) declaredConstructor.newInstance();

                abstractCarFactory.newCar().run();
            } catch (InvocationTargetException | NoSuchMethodException | InstantiationException |
                     IllegalAccessException e) {
                log.error(e.getMessage(), e);
            }
        }


        final String[] allClassName = {
                "cn.blank.pattern.CreationalPatternsFactoryMethodTests$MiniCarFactory"
                , "cn.blank.pattern.CreationalPatternsFactoryMethodTests$RacingCarFactory"
                , "cn.blank.pattern.CreationalPatternsFactoryMethodTests$VanCarFactory"
        };
        for (final String className : allClassName) {
            try {
                final Class<?> aClass = Class.forName(className);
                final Constructor<?> declaredConstructor = aClass.getDeclaredConstructor();
                declaredConstructor.setAccessible(true);
                final AbstractCarFactory abstractCarFactory = (AbstractCarFactory) declaredConstructor.newInstance();

                abstractCarFactory.newCar().run();
            } catch (ClassNotFoundException | InvocationTargetException | NoSuchMethodException |
                     InstantiationException | IllegalAccessException e) {
                e.printStackTrace();
            }
        }


        AbstractCarFactory abstractCarFactory;

        abstractCarFactory = new MiniCarFactory();
        abstractCarFactory.newCar().run();

        abstractCarFactory = new RacingCarFactory();
        abstractCarFactory.newCar().run();

        abstractCarFactory = new VanCarFactory();
        abstractCarFactory.newCar().run();
    }

}