package cn.blank.pattern;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;

import java.util.Map;
import java.util.Objects;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ThreadLocalRandom;

@Slf4j
class CreationalPatternsPrototypeTests {
    /**
     * 创造型模式  --  原型模式
     */

    private static class Person implements Cloneable {
        private String name;
        private int age;

        public Person(final String name, final int age) {
            this.name = name;
            this.age = age;
        }

        public void setName(final String name) {
            this.name = name;
        }

        public void setAge(final int age) {
            this.age = age;
        }

        @Override
        public String toString() {
            return "Person{" + "name='" + name + '\'' + ", age=" + age + '}';
        }

        @Override
        public Person clone() {
            try {
                // 浅拷贝        copy mutable state here, so the clone can't change the internals of the original
                return (Person) super.clone();
            } catch (CloneNotSupportedException e) {
                throw new AssertionError();
            }
        }
    }

    @Slf4j
    private static class CreationalPatternsPrototype {
        private static final Map<String, Person> CACHE_PERSON = new ConcurrentHashMap<>(64);

        Person getPerson(String key) {
            if (Objects.isNull(key)) {
                return null;
            }
            // 从缓存读取
            Person person = CACHE_PERSON.get(key);
            if (Objects.isNull(person)) {
                // 从db获取， 模拟
                person = new Person(key, ThreadLocalRandom.current().nextInt(0, 99));
                CACHE_PERSON.put(key, person);
            }
            // 原型模式
            final Person clone = person.clone();
            log.info("获取到的对象hashcode是{}，得到克隆体对象的hashcode是{}", person.hashCode(), clone.hashCode());
            return clone;
        }

        void printCache() {
            for (final Map.Entry<String, Person> entry : CACHE_PERSON.entrySet()) {
                log.info("key: {}, value: {}", entry.getKey(), entry.getValue());
            }
        }
    }

    @Test
    void testPrototype() {
        final CreationalPatternsPrototype prototype = new CreationalPatternsPrototype();
        final Person person = prototype.getPerson("张三");
        log.info("{}", person);
        if (Objects.nonNull(person)) {
            person.setName("李四");
            person.setAge(99);
            log.info("{}", person);
        }
        log.info("再次获取值看看，{}", prototype.getPerson("张三"));

        prototype.printCache();
    }

}