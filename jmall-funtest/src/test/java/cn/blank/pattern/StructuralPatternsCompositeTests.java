package cn.blank.pattern;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

/**
 * @author _blank    DateTime 2022/7/5 17:46:35
 */
@Slf4j
class StructuralPatternsCompositeTests {
    /* 结构型模式 -- 组合模式 */

    /* 树形菜单就是一个组合模式 */

    private static class Menu {
        private final Integer id;
        private final String name;
        private List<Menu> childs;

        public Menu(final Integer id, final String name) {
            this.id = id;
            this.name = name;
        }

        public Integer getId() {
            return id;
        }

        public String getName() {
            return name;
        }

        public List<Menu> getChilds() {
            return childs;
        }

        void addChildMenu(Menu... menu) {
            if (Objects.isNull(this.childs)) {
                this.childs = new ArrayList<>(Arrays.asList(menu));
            } else {
                this.childs.addAll(Arrays.asList(menu));
            }
        }
    }

    private void printMenu(Menu topMenu) {
        log.info("ID: {}, Name: {}", topMenu.getId(), topMenu.getName());
        final List<Menu> childs = topMenu.getChilds();
        if (childs == null || childs.isEmpty()) {
            return;
        }
        for (final Menu child : childs) {
            this.printMenu(child);
        }
    }

    @Test
    void testComposite() {
        final Menu rootMenu = new Menu(1, "系统管理");
        final Menu roleMenu = new Menu(2, "角色管理");
        final Menu userMenu = new Menu(3, "用户管理");
        final Menu authMenu = new Menu(4, "权限管理");
        rootMenu.addChildMenu(roleMenu, userMenu, authMenu);

        roleMenu.addChildMenu(new Menu(5, "管理员"), new Menu(6, "主管"));
        userMenu.addChildMenu(new Menu(7, "张三"), new Menu(8, "李四"));
        authMenu.addChildMenu(new Menu(9, "a页面"), new Menu(10, "b页面"));

        this.printMenu(rootMenu);
    }
}
