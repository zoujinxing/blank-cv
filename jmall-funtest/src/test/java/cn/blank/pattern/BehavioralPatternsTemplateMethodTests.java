package cn.blank.pattern;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;

/**
 * echo "package cn.blank.pattern;/**@author _blank $((Get-Date).ToString("yyyy-MM-dd HH:mm:ss")) *\/public class BehavioralPatternsTests{/* 行为型模式 --  *\/}" > BehavioralPatternsTests.java
 *
 * @author _blank 2022-07-01 13:03:31
 */
@Slf4j
class BehavioralPatternsTemplateMethodTests {
    /* 行为型模式 -- 模板方法模式 */

    /**
     * 1、定义模板
     */
    private static abstract class CookTemplate {

        /**
         * 定义算法：  定义好了模板
         * 父类可以实现某些步骤
         * 留关键给子类
         * 参考：org.springframework.context.support.AbstractApplicationContext#refresh()
         */
        void cook() {
            //定义算法步骤
            heating();   //v
            addfood();
            addsalt();
            stirfry();   //v
            end();      //v
        }

        //加热方法
        void heating() {
            log.info("开火...");
        }

        //添加食物
        abstract void addfood();

        //加盐
        abstract void addsalt();

        //翻炒
        void stirfry() {
            log.info("翻炒中...");
        }

        //出锅
        void end() {
            log.info("出锅....");
        }
    }

    private static class AutoCookMachine extends CookTemplate {
        @Override
        void addfood() {
            log.info("放了三个小白菜");
        }

        @Override
        void addsalt() {
            log.info("放了三勺盐");
        }
    }

    @Test
    void testTemplateMethod() {
        final CookTemplate cookTemplate = new AutoCookMachine();
        cookTemplate.cook();
    }

}
