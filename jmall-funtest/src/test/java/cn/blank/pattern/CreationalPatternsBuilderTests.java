package cn.blank.pattern;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;

@Slf4j
class CreationalPatternsBuilderTests {
    /**
     * 创造型模式  --  建造者模式
     */

    private static class Phone {
        private String cpu;
        private String mem;
        private String disk;
        private String cam;

        void setCpu(final String cpu) {
            this.cpu = cpu;
        }

        void setMem(final String mem) {
            this.mem = mem;
        }

        void setDisk(final String disk) {
            this.disk = disk;
        }

        void setCam(final String cam) {
            this.cam = cam;
        }

        @Override
        public String toString() {
            return "Phone{" + "cpu='" + cpu + '\'' + ", mem='" + mem + '\'' + ", disk='" + disk + '\'' + ", cam='" + cam + '\'' + '}';
        }
    }

    /**
     * 抽象建造者
     */
    private static class XiaoMiBuilder {
        private final Phone phone;

        XiaoMiBuilder() {
            this.phone = new Phone();
        }

        XiaoMiBuilder customCpu(final String cpu) {
            getPhone().setCpu(cpu);
            return this;
        }

        XiaoMiBuilder customMemory(final String memory) {
            getPhone().setMem(memory);
            return this;
        }

        XiaoMiBuilder customDisk(final String disk) {
            getPhone().setDisk(disk);
            return this;
        }

        XiaoMiBuilder customCamera(final String camera) {
            getPhone().setCam(camera);
            return this;
        }

        Phone getPhone() {
            return phone;
        }
    }

    @Test
    void testBuilder() {
        final Phone xiaomiPhone0 = new XiaoMiBuilder()
                .customCpu("骁龙888")
                .customMemory("8G")
                .customDisk("128G")
                .customCamera("2000万像素")
                .getPhone();
        log.info("{}", xiaomiPhone0);

        final Phone xiaomiPhone1 = new XiaoMiBuilder()
                .customCpu("骁龙666")
                .customMemory("6G")
                .customDisk("64G")
                .customCamera("1300万像素")
                .getPhone();
        log.info("{}", xiaomiPhone1);
    }

}