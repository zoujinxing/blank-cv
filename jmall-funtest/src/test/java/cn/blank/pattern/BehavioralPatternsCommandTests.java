package cn.blank.pattern;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;

/**
 * @author _blank 2022-07-01 13:22:46
 */
@Slf4j
class BehavioralPatternsCommandTests {
    /* 行为型模式 -- 命令模式 */

    /**
     * 小度音箱接受者（命令执行者）。
     */
    private static class XiaoDuSpeakerReceiver {
        void turnOnTheTv() {
            log.info("为您按下了电视的开关键↑");
        }

        void turnOffTheTv() {
            log.info("为您按下了电视的开关键↓");
        }

        void turnUpTheTvVolume() {
            log.info("为您调大了电视的音量");
        }

        void turnDownTheTvVolume() {
            log.info("为您调小了电视的音量");
        }

        void turnOnTheFan() {
            log.info("为您按下了风扇的开关键↑");
        }

        void fanShakingHisHead() {
            log.info("为您按下了风扇的摇头键");
        }

        void singASong() {
            log.info("好的~~ 两只老虎，两只老虎，跑得快~~");
        }

        void stop() {
            log.info("好的~小度告退了！");
        }
    }

    /**
     * 抽象命令类
     */
    interface Command {
        // 命令绑定设备
        XiaoDuSpeakerReceiver receiver = new XiaoDuSpeakerReceiver();

        /**
         * 命令的执行方法
         */
        void execute();
    }

    private static class TvCommand implements Command {
        @Override
        public void execute() {
            log.info("小度小度，打开电视~");
            receiver.turnOnTheTv();

            log.info("小度小度，调大电视音量~");
            receiver.turnUpTheTvVolume();

            log.info("小度小度，调小电视音量！");
            receiver.turnDownTheTvVolume();

            log.info("小度小度，关闭电视~");
            receiver.turnOffTheTv();
        }
    }

    private static class FanCommand implements Command {
        @Override
        public void execute() {
            log.info("小度小度，打开风扇！");
            receiver.turnOnTheFan();

            log.info("小度小度，风扇摇头！");
            receiver.fanShakingHisHead();
        }
    }

    private static class OtherCommand implements Command {
        @Override
        public void execute() {
            log.info("小度小度，唱歌~");
            receiver.singASong();

            log.info("小度小度，别唱了！");
            receiver.stop();
        }
    }

    /**
     * 命令调用者（发起者）
     */
    private static class MasterInvoker {
        Command command;

        MasterInvoker setCommand(final Command command) {
            this.command = command;
            return this;
        }

        void call() {
            //命令
            command.execute();
        }
    }

    @Test
    void testCommand() {
        final MasterInvoker masterInvoker = new MasterInvoker();

        masterInvoker.setCommand(new TvCommand()).call();

        masterInvoker.setCommand(new FanCommand()).call();

        masterInvoker.setCommand(new OtherCommand()).call();
    }

}
