package cn.blank.pattern;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

/**
 * <br/>Date 2022/7/1
 * <br/>Time 14:46:16
 *
 * @author _blank
 */
@Slf4j
class BehavioralPatternsChainOfResponsibilityTests {
    /* 行为型模式 -- 职责链模式 */

    private static class Request {
        String msg; //请求内容

        Request(String msg) {
            this.msg = msg;
        }
    }

    private static class Response {
        String content;

        Response(String content) {
            this.content = content;
        }
    }

    private static class FilterChain {
        private int cursor = 0;
        private final TargetRunnable target;
        private final List<Filter> filterChains;

        FilterChain(final TargetRunnable target, final Filter... filterChains) {
            this.target = target;
            this.filterChains = Arrays.asList(filterChains);
        }

        void doFilter(final Request request, final Response response, final FilterChain chain) {
            if (this.cursor < filterChains.size()) {
                final Filter filter = filterChains.get(this.cursor++);
                filter.doFilter(request, response, chain);
            } else {
                this.target.sayHello();
            }
        }
    }

    private interface Filter {
        void doFilter(Request request, Response response, FilterChain chain);
    }

    private static class CharacterFilter implements Filter {
        @Override
        public void doFilter(Request request, Response response, FilterChain chain) {
            //功能
            request.msg += " ==== ";
            log.info("CharacterFilter...doFilter之前");
            //放行
            chain.doFilter(request, response, chain);
            log.info("CharacterFilter...doFilter之后");
        }
    }

    private static class EncodingFilter implements Filter {
        @Override
        public void doFilter(Request request, Response response, FilterChain chain) {
            request.msg += " oooo ";
            log.info("EncodingFilter...doFilter之前");
            //放行
            chain.doFilter(request, response, chain);
            log.info("EncodingFilter...doFilter之后");
        }
    }

    static class HttpFilter implements Filter {
        @Override
        public void doFilter(Request request, Response response, FilterChain chain) {
            //第一个filter的功能
            request.msg += " >>> ";
            log.info("HttpFilter...doFilter之前");
            //放行
            chain.doFilter(request, response, chain);
            log.info("HttpFilter...doFilter之后");
        }
    }

    private interface TargetRunnable {
        void sayHello();
    }

    private static class HelloWorld implements TargetRunnable {
        @Override
        public void sayHello() {
            log.info("您好，世界！");
        }
    }

    @Test
    void testChainOfResponsibility() {
        final FilterChain filterChain = new FilterChain(new HelloWorld()
                , new CharacterFilter()
                , new EncodingFilter()
                , new HttpFilter());
        final Request req = new Request("req");
        final Response res = new Response("res");
        filterChain.doFilter(req, res, filterChain);
        log.info(req.msg);
        log.info(res.content);
    }

}
