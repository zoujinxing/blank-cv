package cn.blank.pattern;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;

@Slf4j
class CreationalPatternsAbstractFactoryTests {
    /**
     * 创造型模式  --  抽象工厂模式
     */

    /* 定义抽象（抽象类，接口），就会有多实现，多实现自然就有多功能 */

    /* 抽象产品 */
    private static abstract class AbstractCar {
        String engine;

        abstract void run();
    }

    /* 具体产品 */
    private static class RacingCar extends AbstractCar {
        RacingCar() {
            this.engine = "v8发动机";
        }

        @Override
        void run() {
            log.info("{}，嗖嗖嗖……", this.engine);
        }
    }

    /* 具体产品 */
    private static class VanCar extends AbstractCar {
        VanCar() {
            this.engine = "单杠柴油机";
        }

        @Override
        void run() {
            log.info("{}，嗒嗒嗒……", this.engine);
        }
    }

    // -----------------------------------------------------------------------------------------------------------------

    /* 抽象产品 */
    private static abstract class AbstractMask {
        Integer price;

        abstract void protectedMe();
    }

    /* 具体产品 */
    private static class CommonMask extends AbstractMask {
        CommonMask() {
            price = 1;
        }

        @Override
        void protectedMe() {
            log.info("普通口罩..价格{}元..简单保护...请及时更换", this.price);
        }
    }

    /* 具体产品 */
    private static class N95Mask extends AbstractMask {
        N95Mask() {
            this.price = 2;
        }

        @Override
        void protectedMe() {
            log.info("N95口罩..价格{}元..超级防护", this.price);
        }
    }

    // -----------------------------------------------------------------------------------------------------------------

    /* wulin 总厂规范 */
    private static abstract class WulinFactory {
        abstract AbstractCar newCar();

        abstract AbstractMask newMask();
    }

    // -----------------------------------------------------------------------------------------------------------------

    /* wulin 汽车工厂 */
    private static abstract class WulinCarFactory extends WulinFactory {
        @Override
        abstract AbstractCar newCar();

        @Override
        AbstractMask newMask() {
            return null;
        }
    }

    /* 汽车具体工厂，只造赛车 */
    private static class WulinRacingCarFactory extends WulinCarFactory {
        @Override
        AbstractCar newCar() {
            return new RacingCar();
        }
    }

    /* 汽车具体工厂，只造面包车 */
    private static class WulinVanCarFactory extends WulinCarFactory {
        @Override
        AbstractCar newCar() {
            return new VanCar();
        }
    }

    // -----------------------------------------------------------------------------------------------------------------

    /* wulin 口罩工厂 */
    private static abstract class WulinMaskFactory extends WulinFactory {
        @Override
        AbstractCar newCar() {
            return null;
        }

        abstract AbstractMask newMask();
    }

    /* 口罩具体工厂，杭州口罩厂 */
    private static class WulinHangZhouMaskFactory extends WulinMaskFactory {
        @Override
        AbstractMask newMask() {
            return new CommonMask();
        }
    }

    /* 口罩具体工厂，武汉口罩厂 */
    private static class WulinWuHanMaskFactory extends WulinMaskFactory {
        @Override
        AbstractMask newMask() {
            return new N95Mask();
        }
    }

    // -----------------------------------------------------------------------------------------------------------------

    @Test
    void testAbstractFactory() {
        WulinFactory wulinFactory;

        wulinFactory = new WulinRacingCarFactory();
        wulinFactory.newCar().run();
        wulinFactory = new WulinVanCarFactory();
        wulinFactory.newCar().run();

        wulinFactory = new WulinHangZhouMaskFactory();
        wulinFactory.newMask().protectedMe();
        wulinFactory = new WulinWuHanMaskFactory();
        wulinFactory.newMask().protectedMe();
    }

}