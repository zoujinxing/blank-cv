package cn.blank.pattern;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;

/**
 * @author _blank    DateTime 2022/7/5 17:45:11
 */
@Slf4j
class StructuralPatternsBridgeTests {
    /* 结构型模式 -- 桥接模式 */

    private static abstract class AbstractPhone {
        /* 桥接 */
        AbstractSale sale;

        abstract String getPhone();

        void setSale(final AbstractSale sale) {
            this.sale = sale;
        }
    }

    private static class IPhone extends AbstractPhone {
        @Override
        String getPhone() {
            return this.getClass().getSimpleName() + "    " + super.sale.getSaleInfo();
        }
    }

    private static class MiPhone extends AbstractPhone {
        @Override
        String getPhone() {
            return this.getClass().getSimpleName() + "    " + super.sale.getSaleInfo();
        }
    }

    private static abstract class AbstractSale {
        private final String type;
        private final Integer price;

        AbstractSale(final String type, final Integer price) {
            this.type = type;
            this.price = price;
        }

        String getSaleInfo() {
            return "销售渠道：" + this.type + "，价格：" + this.price;
        }
    }

    private static class OfflineSales extends AbstractSale {
        OfflineSales(final String type, final Integer price) {
            super(type, price);
        }
    }

    private static class OnlineSales extends AbstractSale {
        OnlineSales(final String type, final Integer price) {
            super(type, price);
        }
    }

    private static class StudentVersion extends AbstractSale {
        StudentVersion(final String type, final Integer price) {
            super(type, price);
        }
    }

    @Test
    void testBridge() {
        final IPhone iPhone = new IPhone();

        iPhone.setSale(new OnlineSales("线上", 8799));
        log.info("{}", iPhone.getPhone());

        iPhone.setSale(new OfflineSales("线下", 9799));
        log.info("{}", iPhone.getPhone());

        iPhone.setSale(new StudentVersion("学生", 6799));
        log.info("{}", iPhone.getPhone());

        final MiPhone miPhone = new MiPhone();
        miPhone.setSale(new OnlineSales("官网", 1999));
        log.info("{}", miPhone.getPhone());
    }

}
