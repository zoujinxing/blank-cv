package cn.blank.pattern;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;

import java.util.concurrent.CountDownLatch;

@Slf4j
class CreationalPatternsSingletonTest {
    /**
     * 创建型模式  --  单例模式
     */
    private static class CreationalPatternsSingleton {
        private static volatile CreationalPatternsSingleton singleton;

        private CreationalPatternsSingleton() {
        }

        static CreationalPatternsSingleton getInstance() {
            if (singleton == null) {
                synchronized (CreationalPatternsSingleton.class) {
                    if (singleton == null) {
                        singleton = new CreationalPatternsSingleton();
                    }
                }
            }
            return singleton;
        }

        void sayHello() {
            log.info("methodName: sayHello, {}    {}", "您好世界", this.hashCode());
        }
    }

    @Test
    void testSingleton() {
        final int countDown = 128;
        final CountDownLatch latch = new CountDownLatch(countDown);
        for (int i = 0; i < countDown; i++) {
            new Thread(() -> {
                try {
                    CreationalPatternsSingleton.getInstance().sayHello();
                } finally {
                    latch.countDown();
                }
            }, "第" + i + "个线程").start();
        }
        try {
            latch.await();
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
            log.error(e.getMessage(), e);
        }
    }

}

