package cn.blank.pattern;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;

import java.util.Objects;

@Slf4j
class CreationalPatternsSimpleFactoryTests {
    /**
     * 创造型模式  --  简单工厂模式
     */

    private static abstract class AbstractEnergyCar {
        private final String carModel;

        public AbstractEnergyCar(final String carModel) {
            this.carModel = carModel;
        }

        public String getCarModel() {
            return carModel;
        }

        abstract void run();
    }

    private static class ElectricVehicle extends AbstractEnergyCar {
        ElectricVehicle() {
            super("EV");
        }

        @Override
        void run() {
            log.info("{}只有电池提供能源供给，通过电动机提供动力，并且由于没有变速箱的存在，在加速时不仅没有任何顿挫感，只要给油就是最大扭矩。", super.getCarModel());
        }
    }

    private static class HybridElectricVehicle extends AbstractEnergyCar {
        HybridElectricVehicle() {
            super("HEV");
        }

        @Override
        void run() {
            log.info("{}混合动力车型的电池小还不需要外接充电，因此不需要有续航焦虑存在。通过回收制动能量可以改善车辆低速动力输出。", super.getCarModel());
        }
    }

    private static class PluginHybridElectricVehicle extends AbstractEnergyCar {
        PluginHybridElectricVehicle() {
            super("PHEV");
        }

        @Override
        void run() {
            log.info("{}介于纯电动与燃油车之间，此类车型的电池容量较大，有专门的充电插口。当车辆电能不足时发动机会介入给电到动力电池，同时它还有着电动车型的优点，完全可以作为一款纯电动车型使用。", super.getCarModel());
        }
    }

    private static class EnergyCar {
        static AbstractEnergyCar getEnergyCar(String carModel) {
            switch (carModel) {
                case "EV":
                    return new ElectricVehicle();
                case "HEV":
                    return new HybridElectricVehicle();
                case "PHEV":
                    return new PluginHybridElectricVehicle();
                default:
                    log.info("没有获取到您的新能源车类型！");
                    return null;
            }
        }
    }

    @Test
    void testSimpleFactory() {
        final String[] carModels = {"EV", "HEV", "PHEV", "ACK"};
        for (final String carModel : carModels) {
            final AbstractEnergyCar energyCar = EnergyCar.getEnergyCar(carModel);
            if (Objects.nonNull(energyCar)) {
                energyCar.run();
            }
        }
    }

}