package cn.blank.pattern;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;

/**
 * @author _blank 2022-07-01 13:11:26
 */
@Slf4j
class BehavioralPatternsStrategyTests {
    /* 行为型模式 -- 策略模式 */

    /**
     * 游戏策略
     */
    private interface GameStrategy {
        //战斗策略
        void warStrategy();
    }

    private static class RandomStrategy implements GameStrategy {
        @Override
        public void warStrategy() {
            log.info("大乱斗...");
        }
    }

    /**
     * 稳健运营策略
     */
    private static class SteadyStrategy implements GameStrategy {
        @Override
        public void warStrategy() {
            log.info("各路小心...及时支援...");
        }
    }

    /**
     * 冲锋向前策略
     */
    private static class UziStrategy implements GameStrategy {
        @Override
        public void warStrategy() {
            log.info("勇往直前.....");
        }
    }

    /**
     * 环境类
     */
    private static class TeamGNR {

        //抽取游戏策略算法，并进行引用
        private GameStrategy gameStrategy;

        void setGameStrategy(GameStrategy gameStrategy) {
            this.gameStrategy = gameStrategy;
        }

        void startGame() {
            log.info("游戏开始.....");
            //游戏策略
            //
            gameStrategy.warStrategy();
            log.info("win......");
        }
    }

    @Test
    void testStrategy() {
        final TeamGNR teamGNR = new TeamGNR();

        teamGNR.setGameStrategy(new RandomStrategy());
        teamGNR.startGame();

        teamGNR.setGameStrategy(new SteadyStrategy());
        teamGNR.startGame();

        teamGNR.setGameStrategy(new UziStrategy());
        teamGNR.startGame();

    }

}
