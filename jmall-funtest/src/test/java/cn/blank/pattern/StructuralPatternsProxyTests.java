package cn.blank.pattern;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.cglib.proxy.Enhancer;
import org.springframework.cglib.proxy.MethodInterceptor;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

/**
 * @author _blank    DateTime 2022/7/5 17:39:56
 */
@Slf4j
class StructuralPatternsProxyTests {
    /* 结构型模式 -- 代理模式 */

    // -----------------------------------------------------------------------------------------------------------------
    // https://mp.weixin.qq.com/s/PDeE329ngo4bui-688PoPg    https://juejin.cn/post/7053793796277927943
    // cglib 动态代理
    private static class ZhaoLiuTikTok {
        // 在静态匿名内部类中，没有显示构造函数，会抛异常。
        ZhaoLiuTikTok() {
        }

        void liveStreaming() {
            log.info("我是赵六，我正在直播……");
        }

        void sell() {
            log.info("我是赵六，我正在直播卖货……");
        }

        @SuppressWarnings({"unchecked", "SameParameterValue"})
        static <T> T getProxyInstance(Class<T> clazz) {
            return (T) Enhancer.create(clazz, (MethodInterceptor) (o, method, objects, methodProxy) -> {
                try {
                    log.info("cglib执行代理方法前……");
                    return methodProxy.invokeSuper(o, objects);
                } finally {
                    log.info("cglib执行代理方法后……");
                }
            });
        }
    }

    @Test
    void testCglibProxy() {
        final ZhaoLiuTikTok proxyZhaoLiuTikTok = ZhaoLiuTikTok.getProxyInstance(ZhaoLiuTikTok.class);
        proxyZhaoLiuTikTok.liveStreaming();
        log.info("……………………………………………………………………………………………………………………");
        proxyZhaoLiuTikTok.sell();
    }

    // -----------------------------------------------------------------------------------------------------------------
    // java jdk动态代理
    private interface TikTokExt extends TikTok {
        /* 直播卖货 */
        void sell();
    }

    private static class JdkProxy<T> implements InvocationHandler {
        private final T target;

        JdkProxy(final T target) {
            this.target = target;
        }

        static <T> T getProxy(T target) {
            final Object proxyInstance = Proxy.newProxyInstance(
                    target.getClass().getClassLoader()
                    , target.getClass().getInterfaces()
                    , new JdkProxy<>(target)
            );
            //noinspection unchecked
            return (T) proxyInstance;
        }

        @Override
        public Object invoke(final Object proxy, final Method method, final Object[] args) throws Throwable {
            log.info("method环绕前");
            final Object invoke = method.invoke(target, args);
            log.info("method环绕后");
            return invoke;
        }
    }

    private static class WangWuTiktok implements TikTokExt {
        @Override
        public void liveStreaming() {
            log.info("我是王五，我正在直播……");
        }

        @Override
        public void sell() {
            log.info("我是王五，我正在直播卖货……");
        }
    }

    @Test
    void testDynamicProxy() {
        final TikTokExt tikTok = new WangWuTiktok();
        final TikTokExt proxyTiktok = JdkProxy.getProxy(tikTok);
        proxyTiktok.liveStreaming();
        proxyTiktok.sell();

        // ------------------------------------------
        log.info("分割线…………………………………………………………………………");

        final Object proxyInstance = Proxy.newProxyInstance(tikTok.getClass().getClassLoader(), tikTok.getClass().getInterfaces(), (proxy, method, args) -> {
            try {
                log.info("执行代理方法前……");
                return method.invoke(tikTok, args);
            } finally {
                log.info("执行代理方法后……");
            }
        });
        final TikTokExt tikTokExt = (TikTokExt) proxyInstance;
        tikTokExt.liveStreaming();
        tikTokExt.sell();
    }

    // -----------------------------------------------------------------------------------------------------------------
    // java 静态代理

    // 抖音直播
    private interface TikTok {
        void liveStreaming();
    }

    private static class ZhangSanTiktok implements TikTok {
        @Override
        public void liveStreaming() {
            log.info("我是张三，我正在直播……");
        }
    }

    private static class LiSiTiktokProxy implements TikTok {
        private final TikTok tikTok;

        LiSiTiktokProxy(final TikTok tikTok) {
            this.tikTok = tikTok;
        }

        @Override
        public void liveStreaming() {
            log.info("我是李四，我开始介入{}的直播……", this.tikTok.getClass().getSimpleName());
            this.tikTok.liveStreaming();
            log.info("我是李四，我已经退出{}的直播……", this.tikTok.getClass().getSimpleName());
        }
    }

    @Test
    void testStaticProxy() {
        final TikTok tikTok = new LiSiTiktokProxy(new ZhangSanTiktok());
        tikTok.liveStreaming();
    }

}
