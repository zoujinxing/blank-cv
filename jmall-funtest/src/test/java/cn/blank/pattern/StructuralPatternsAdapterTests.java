package cn.blank.pattern;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;

/**
 * @author _blank    DateTime 2022/7/5 17:44:12
 */
@Slf4j
class StructuralPatternsAdapterTests {
    /* 结构型模式 -- 适配器模式 */

    private interface Player {
        String play();
    }

    private static class MoviePlayer implements Player {
        @Override
        public String play() {
            log.info("正在播放：Java从入门到放弃.mp4");
            return "空你几哇";
        }
    }

    /* 组合方式 */
    private static class JapanMovieAdapter implements Player {
        private final Translator translator = new ZhTranslator();
        private final Player target;

        JapanMovieAdapter(final Player target) {
            this.target = target;
        }

        @Override
        public String play() {
            final String content = this.target.play();
            final String translate = this.translator.translate(content);
            log.info("翻译（日语转中文）：{}", translate);
            return content;
        }
    }

    /* 继承方式 */
    private static class JapanMovieAdapterExtends extends ZhTranslator implements Player {
        private final Player target;

        JapanMovieAdapterExtends(final Player target) {
            this.target = target;
        }

        @Override
        public String play() {
            final String content = this.target.play();
            final String translate = super.translate(content);
            log.info("翻译（日语转中文）：{}", translate);
            return content;
        }
    }

    private interface Translator {
        String translate(String content);
    }

    private static class ZhTranslator implements Translator {
        @Override
        public String translate(final String content) {
            if ("空你几哇".equals(content)) {
                return "您好";
            } else if ("阿里嘎多".equals(content)) {
                return "您好";
            } else if ("纳尼".equals(content)) {
                return "什么";
            } else if ("撒哟那拉".equals(content)) {
                return "再见";
            }
            return null;
        }
    }

    @Test
    void testAdapter() {
        Player movie = new JapanMovieAdapter(new MoviePlayer());
        final String play = movie.play();
        log.info("电影原文：{}", play);

        final JapanMovieAdapterExtends japanMovieAdapterExtends = new JapanMovieAdapterExtends(new MoviePlayer());
        final String playContent = japanMovieAdapterExtends.play();
        log.info("电影原文(继承方式)：{}", play);
    }

}
