package cn.blank.pattern;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;

/**
 * @author _blank    DateTime 2022/7/5 17:45:38
 */
@Slf4j
class StructuralPatternsDecoratorTests {
    /* 结构型模式 -- 装饰模式 */

    private interface Tiktok {
        void liveStreaming();
    }

    /* 装饰器 */
    private static class BeautyTiktokDecorator implements Tiktok {
        private final Tiktok tiktok;

        BeautyTiktokDecorator(final Tiktok tiktok) {
            this.tiktok = tiktok;
        }

        @Override
        public void liveStreaming() {
            this.enhance();
            this.tiktok.liveStreaming();
        }

        /* 装饰器核心，关心增强这个类的方法 */
        void enhance() {
            log.info("为您开启了美颜！");
        }
    }

    private static class ZhangSanTiktok implements Tiktok {
        @Override
        public void liveStreaming() {
            log.info("张三正在直播...");
        }
    }

    private static class LiSiTiktok implements Tiktok {
        @Override
        public void liveStreaming() {
            log.info("李四正在直播...");
        }
    }

    @Test
    void testDecorator() {
        final Tiktok zhangSanTiktok = new ZhangSanTiktok();
        zhangSanTiktok.liveStreaming();

        BeautyTiktokDecorator beautyTiktokDecorator = new BeautyTiktokDecorator(zhangSanTiktok);
        beautyTiktokDecorator.liveStreaming();

        final LiSiTiktok liSiTiktok = new LiSiTiktok();
        liSiTiktok.liveStreaming();

        beautyTiktokDecorator = new BeautyTiktokDecorator(liSiTiktok);
        beautyTiktokDecorator.liveStreaming();
    }
}
