package cn.blank.pattern;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

/**
 * @author _blank    DateTime 2022/7/5 17:38:00
 */
@Slf4j
class BehavioralPatternsInterpreterTests {
    /* 行为型模式 -- 解释器模式 */

    /* 解析表达式 */
    private static abstract class IDCardExpression {
        abstract boolean interpret(String expression);
    }

    private static class OrExpression extends IDCardExpression {
        private final IDCardExpression cityExpression;
        private final IDCardExpression typeExpression;

        OrExpression(final IDCardExpression cityExpression, final IDCardExpression typeExpression) {
            if (cityExpression instanceof OrExpression || typeExpression instanceof OrExpression) {
                throw new IllegalArgumentException("不能传入自身");
            }
            this.cityExpression = cityExpression;
            this.typeExpression = typeExpression;
        }

        @Override
        boolean interpret(final String expression) {
            return this.cityExpression.interpret(expression) || this.typeExpression.interpret(expression);
        }
    }

    private static class TerminalExpression extends IDCardExpression {
        private final String symbol;
        private final Set<String> containsData;

        TerminalExpression(final String symbol, final String... containsData) {
            this.symbol = symbol;
            this.containsData = new HashSet<>(Arrays.asList(containsData));
        }

        /* 解析 */
        @Override
        boolean interpret(final String expression) {
            final String[] splitStr = expression.split(this.symbol);
            for (final String s : splitStr) {
                if (this.containsData.contains(s)) {
                    return true;
                }
            }
            return false;
        }
    }

    @Test
    void testInterpreter() {
        final TerminalExpression cityExpr = new TerminalExpression(":", "北京市", "上海市", "广州市", "深圳市");
        final TerminalExpression typeExpr = new TerminalExpression("-", "医生", "护士", "老人", "儿童");

        final IDCardExpression idCardExpression = new OrExpression(cityExpr, typeExpr);
        final String[] str = {"广州市:张三丰-程序员", "深圳市:李思思-护士", "河源市:王五万-售票员"};
        for (final String s : str) {
            if (idCardExpression.interpret(s)) {
                log.info("{}，免票入场", s);
            } else {
                log.info("{}，请购票入场", s);
            }
        }
    }
}
