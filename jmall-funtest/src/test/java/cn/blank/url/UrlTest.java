package cn.blank.url;

import lombok.extern.slf4j.Slf4j;

import java.net.MalformedURLException;
import java.net.URL;

/**
 * <br/>Date 2022/5/10
 * <br/>Time 14:56:06
 *
 * @author _blank
 */
@Slf4j
public class UrlTest {

    public static void main(String[] args) {
        try {
            final URL url1 = new URL("http://mysql.jmall.com");

            final URL url2 = new URL("http://redis.jmall.com");

            final URL url3 = new URL("http://cqy.jmall.com");

            System.out.println(url1.equals(url2));

            System.out.println(url2.equals(url3));
        } catch (MalformedURLException e) {
            log.error(e.getMessage(), e);
        }
    }

}
